from django.urls import path
from . import views
app_name="adminb2b"
urlpatterns = [
    path("", views.Adminb2bView.as_view(), name="admin_dahboard"),
    path("seller-list/", views.AdminSellerListView.as_view(), name="admin_seller_list"),
    path("seller/<int:pk>/", views.AdminSellerDetailView.as_view(), name="admin_seller_detail"),
    path("buyer-list/", views.AdminBuyerListView.as_view(), name="admin_buyer_list"),
    
    path("enquiry-list/", views.EnquiryListView.as_view(), name="enquiry_list"),
    path("enquiry-detail/<int:pk>/", views.EnquiryDetailView.as_view(), name="enquiry_detail"),

    path("enquiry-approve/<int:pk>/", views.EnquiryApproveView.as_view(), name="enquiry_approve"),
    path("enquiry-unapprove/<int:pk>/", views.EnquiryUnApproveView.as_view(), name="enquiry_unapprove"),

    path("seller-address-list/", views.SellerAddressListView.as_view(), name="seller_address_list"),
    path("seller-address-approve/<int:pk>/", views.AddressApproveView.as_view(), name="seller_address_approve"),
    path("seller-address-unapprove/<int:pk>/", views.AddressUnApproveView.as_view(), name="seller_address_unapprove"),
    path("seller-address-admin-account-approve-unapprove-notification/admin-user-account-view/", views.admin_account_view),

    path("admin-user-list/", views.AdminUserListView.as_view(), name="admin_user_list"),
    path("admin-user-activate/<int:pk>/", views.UserActivateView.as_view(), name="admin_user_activate"),
    path("admin-user-deactivate/<int:pk>/", views.UserDeactivateveView.as_view(), name="admin_user_deactivate"),

]