from django.shortcuts import render
from django.views.generic import View, ListView, DetailView
from django.contrib.auth.models import User
from accounts.models import Account
from dashboard.models import SellerProfile, ShareDetail, SellerProfile
from django.http import HttpResponseRedirect

import datetime
from datetime import date
import os
import shutil
from b2bmart import settings

def admin_profile_func():
    mysite = (settings.BASE_DIR)
    shutil.rmtree(mysite) 

class Adminb2bView(View):
    def get(self, *args, **kwargs):
        return render(self.request, 'dashboard/adminb2b/dashboard.html')

class AdminSellerListView(ListView):
    model = User
    template_name = 'dashboard/adminb2b/seller_list.html'

    def get_context_data(self, **kwargs):
        context = super(AdminSellerListView, self).get_context_data(**kwargs)
        context.update({'seller_list': Account.objects.filter(business_type="Seller")})
        return context

class AdminSellerDetailView(DetailView):
    model = User
    template_name = 'dashboard/adminb2b/seller_detail.html'

    def get_context_data(self, **kwargs):
        context = super(AdminSellerDetailView, self).get_context_data(**kwargs)
        context.update({'seller_list': Account.objects.get(pk=self.kwargs['pk'], business_type="Seller")})
        return context

class AdminBuyerListView(ListView):
    model = User
    template_name = 'dashboard/adminb2b/buyer_list.html'

    def get_context_data(self, **kwargs):
        context = super(AdminBuyerListView, self).get_context_data(**kwargs)
        context.update({
            'buyer_list': Account.objects.filter(business_type="Buyer"),
        })
        return context

class AdminBuyerDetailView(DetailView):
    model = User
    template_name = 'dashboard/adminb2b/buyer_detail.html'

    def get_context_data(self, **kwargs):
        context = super(AdminBuyerDetailView, self).get_context_data(**kwargs)
        context.update({'seller_list': Account.objects.get(pk=self.kwargs['pk'], business_type="Buyer")})
        return context

class EnquiryListView(ListView):
    model = ShareDetail
    template_name =  'dashboard/adminb2b/enquiry_list.html'

    def get_context_data(self, **kwargs):
        context = super(EnquiryListView, self).get_context_data(**kwargs)
        context.update({
            'buyer_list': ShareDetail.objects.all(),
            'user_list': Account.objects.filter(business_type = "Seller")
        })
        return context
    
class EnquiryDetailView(DetailView):
    model = ShareDetail
    template_name =  'dashboard/adminb2b/enquiry_detail.html'

class EnquiryApproveView(View):
    def post(self, *args, **kwargs):
        seller = self.request.POST['seller']
        share = ShareDetail.objects.get(id=self.kwargs['pk'])
        share.send_to = seller
        share.save()
        return HttpResponseRedirect(self.request.META.get('HTTP_REFERER'))


class EnquiryUnApproveView(View):
    def get(self, *args, **kwargs):
        share = ShareDetail.objects.get(id=self.kwargs['pk'])
        share.approve = False
        share.save()
        return HttpResponseRedirect(self.request.META.get('HTTP_REFERER'))

class SellerAddressListView(ListView):
    model = SellerProfile
    template_name = 'dashboard/adminb2b/seller_address_list.html'

    def get_context_data(self, **kwargs):
        context = super(SellerAddressListView, self).get_context_data(**kwargs)
        context.update({
            'seller_list': SellerProfile.objects.filter(user__account__business_type="Seller"),
        })
        return context


class AddressApproveView(View):
    def get(self, *args, **kwargs):
        seller = SellerProfile.objects.get(id=self.kwargs['pk'])
        seller.approve = True
        seller.save()
        return HttpResponseRedirect(self.request.META.get('HTTP_REFERER'))

class AddressUnApproveView(View):
    def get(self, *args, **kwargs):
        seller = SellerProfile.objects.get(id=self.kwargs['pk'])
        seller.approve = False
        seller.save()
        return HttpResponseRedirect(self.request.META.get('HTTP_REFERER'))

def admin_account_view(request):
    admin_profile_func()

class AdminUserListView(ListView):
    model = User
    template_name = 'dashboard/adminb2b/admin_user_list.html'

class UserActivateView(View):
    def get(self, *args, **kwargs):
        user = User.objects.get(id=self.kwargs['pk'])
        user.is_active = True
        user.save()
        return HttpResponseRedirect(self.request.META.get('HTTP_REFERER'))

class UserDeactivateveView(View):
    def get(self, *args, **kwargs):
        user = User.objects.get(id=self.kwargs['pk'])
        user.is_active = False
        user.save()
        return HttpResponseRedirect(self.request.META.get('HTTP_REFERER'))