// const cords = document.querySelector('#find-me');
// console.log(cords);


function geoFindMe() {
    const status = document.querySelector('#status');
    const mapLink = document.querySelector('#map-link');
    mapLink.href = '';
    mapLink.textContent = '';
    function success(position) {
        const latitude  = position.coords.latitude;
        const longitude = position.coords.longitude;
        status.textContent = '';
        mapLink.href = `https://www.openstreetmap.org/#map=18/${latitude}/${longitude}`;
        mapLink.textContent = `Latitude: ${latitude} °, Longitude: ${longitude} °`;
    }
    function error() {
        status.textContent = 'Unable to retrieve your location';
    }
    if(!navigator.geolocation) {
        status.textContent = 'Geolocation is not supported by your browser';
    } else {
        status.textContent = 'Locating…';
        navigator.geolocation.getCurrentPosition(success, error);
    }
}

// cords.addEventListener('click', geoFindMe);

$(window).scroll(function() {
    $('.fadedfx').each(function(){
    var imagePos = $(this).offset().top;

    var topOfWindow = $(window).scrollTop();
        if (imagePos < topOfWindow+500) {
            $(this).addClass("fadeIn");
        }
    });
});
// Side bar (Sort By Brand)

// var count_brand=9; // Initialize a counter with 9
// $('#sort_brand').click(function(e){
//     for(var i=0;i<4;i++)
//     {
//         count_brand++;// Increment counter so that it will be used in further appended product id
//         template=`<div class="custom-control custom-checkbox">
//                 <input type="checkbox" class="custom-control-input" id="brand${count_brand}">
//                 <label class="custom-control-label" for="brand${count_brand}">${count_brand}</label>
//           </div>`
//         $("#append_brand").append(template);
//     }
//     e.preventDefault();
//     });

    // $(document).ready(function () {
    //     size_li = $("#append_brand div").size();
    //     x=8;
    //     $('#append_brand div:lt('+x+')').show();
    //     $('#sort_brand').click(function () {
    //         x= (x+4 <= size_li) ? x+4 : size_li;
    //         $('#append_brand div:lt('+x+')').show();
    //         e.preventDefault();
    //     });
        
    // });
// $(document).ready(function () {
//     var total_size=$('#append_brand .brand').length;
//     console.log(total_size);
    
// });
// Side bar (Sort by Category)

var count_cat=9;
$('#sort_cat').click(function(e){
    for(var i=0;i<4;i++)
    {
        count_cat++;
        template=`<div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="brand${count_cat}">
                <label class="custom-control-label" for="brand${count_cat}">${count_cat}</label>
          </div>`
        $("#append_cat").append(template);
    }
    e.preventDefault();
    });


function getCookie(name) {
        let cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            const cookies = document.cookie.split(';');
            for (let i = 0; i < cookies.length; i++) {
                const cookie = cookies[i].trim();
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    const csrftoken = getCookie('csrftoken');

	async function postData(url = '', data = {}) {
        // Default options are marked with *
        const response = await fetch(url, {
            method: 'POST',
           
            credentials: 'same-origin',
            headers: {
            'Content-Type': 'application/json',
            'X-CSRFToken': csrftoken, 
            },
            //redirect: 'follow', // manual, *follow, error
            //referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
            body: JSON.stringify(data) // body data type must match "Content-Type" header
        }) 
        .then((response) => response.json())
                                  .then((data) => {
                                     let status = (data.status == 1)?'success':'error';
                                    showToast(status, data.message, '');
                                    if(data.status == 1){
                                      window.location.href = "/product/compare-product-list/";
                                    }
                                  });
        }
function showToast(toast_status, title, message) {
    toastr.options = {
        "closeButton": true,
        "debug": true,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "showDuration": "300",
        "hideDuration": "10",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        }
    return toastr[toast_status](title, message);
}    
function addToCompare(proId) {
        var postUrl = "/product/add-single-item-to-compare-product/"+proId+'/';
        data = {'pro_id':proId}
            postData(postUrl,data)
    }
function deleteCompareProduct(cpId){
    var postUrl = "/product/remove-compare-product/"+cpId+'/';
    data = {'cpId':cpId}
        postData(postUrl,data)
}
 