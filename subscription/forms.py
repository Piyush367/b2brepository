from django import forms
from .models import Subscription, SubscriptionNewsletter

class SubscriptionNewsletterForm(forms.ModelForm):
    email = forms.CharField(widget=forms.EmailInput( attrs={'class': 'form-control'}), required=False)
    class Meta:
        model = SubscriptionNewsletter
        fields = ['email']