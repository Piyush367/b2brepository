from django.shortcuts import render, redirect
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth.decorators import login_required
from django.views.generic import View
from django.views.decorators.csrf import csrf_exempt
import datetime
import random
from django.utils import timezone
import string
from PayTm import Checksum

from django.contrib import messages

from .models import Subscription
from .forms import SubscriptionNewsletterForm
from product.models import Payment
from datetime import date
from dateutil.relativedelta import relativedelta
from django.core.exceptions import ObjectDoesNotExist

mid = 'fGrIzA97726764689450'
merchent_key ='NjeNJa4DJmq53!yu'

class PaidServiceView(LoginRequiredMixin, UserPassesTestMixin, View):
    def test_func(self):
        return self.request.user.is_authenticated

    def get(self,request, *args, **kwargs):
        return render(request, 'subscription/certificate.html')


class SubscriptionCheckoutView(LoginRequiredMixin, UserPassesTestMixin, View):
    def get(self,*args, **kwargs):
        return render(self.request, 'dashboard/subscription_checkout.html')

    def test_func(self):
        return self.request.user.is_authenticated

    def post(self,*args, **kwargs):
        amount = self.request.POST['vishwas']
        if amount == "0":
            messages.success(self.request, "you have got free subscription")
            return redirect("/")
        current_time = str(datetime.datetime.now())
        try:
            sub = Subscription.objects.filter(user=self.request.user).last()
            if sub is None:
                sub=1
            else:
                sub = int(sub.id)+1
        except ObjectDoesNotExist:
            sub=1
        try:
            updated_id = ''.join([current_time[:10],current_time[11:13],current_time[14:16],current_time[17:], str(sub.id)])
        except:
            updated_id = ''.join([current_time[:10],current_time[11:13],current_time[14:16],current_time[17:], str(sub)])

        param_dict = {
            'MID': mid,
            'ORDER_ID': str(updated_id),
            'TXN_AMOUNT': str(amount),
            'CUST_ID': self.request.user.email,
            'INDUSTRY_TYPE_ID': 'Retail',
            'WEBSITE': 'WEBSTAGING',
            'CHANNEL_ID': 'WEB',
            'CALLBACK_URL': 'http://127.0.0.1:8000/subscription/handlerequest/'
        }
        param_dict['CHECKSUMHASH'] = Checksum.generate_checksum(param_dict, merchent_key)
        return render(self.request, 'paytm/paytm.html', {'param_dict': param_dict})

@csrf_exempt
def handlerequest(request):
    # paytm will send you post request here
    form = request.POST
    response_dict = {}
    for i in form.keys():
        response_dict[i] = form[i]
    return render(request, 'subscription/paytm/paymentstatus.html', {'response': response_dict})

@csrf_exempt
@login_required
def back(request):
    if request.method == 'POST':
        form = request.POST
        response_dict = {}
        for i in form.keys():
            response_dict[i] = form[i]
            if i == 'CHECKSUMHASH':
                checksum = form[i]
        verify = Checksum.verify_checksum(response_dict, merchent_key, checksum)

        if verify:
            if response_dict['RESPCODE'] == '01':
                # create the payment
                print("inside backkkkkkkkkkkkkkkkkkkkkkkkkkkk verify")
                payment = Payment()
                order_id_post = form['ORDERID']
                in_order_id_post = int(order_id_post[23:])
                payment.order_id = in_order_id_post
                payment.txn_id = form['TXNID']
                payment.txn_amount = form['TXNAMOUNT']
                payment.pmt_mode = form['PAYMENTMODE']
                payment.txn_date = form['TXNDATE']
                payment.txn_status = form['STATUS']
                payment.user = request.user
                obj = Payment.objects.filter(user=request.user, order_id=int(order_id_post[23:]))
                print("orderrrrrrrrrrrrrrrrr id ", int(order_id_post[23:]))
                if not obj.exists():
                    print("typeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee", type(payment.txn_amount), payment.txn_amount)
                    a = int(float(payment.txn_amount))
                    print(type(a), a)
                    payment.save()
                    
                        
                    subscription = Subscription()
                    subscription.user = request.user
                    subscription.payment = payment

                    if payment.txn_amount == "3000.00":
                        subscription.subscription_name = "VISHWAS"

                    if payment.txn_amount == "6000.00":
                        subscription.subscription_name = "VYAPAR"

                    subscription.subscription_amount = payment.txn_amount
                    subscription.subscribed_date =  datetime.datetime.now()
                    subscription.expiry = date.today() + relativedelta(months=+1)
                    subscription.status = True
                    subscription.save()
                    messages.info(request, 'Your subscription is successful')
            else:
                print('order was not successful because' + response_dict['RESPMSG'])
                messages.warning(request, 'Your Order is Failure')
        print("outsideeeeeeeeeeeeeeeeeeeeeeeeeeeee", response_dict)
        return render(request, 'paytm/back.html',{'response':response_dict})
    return redirect('dashboard:cart')


class SubscriptionNewsletterView(View):
    def post(self, *args, **kwargs):
        form = SubscriptionNewsletterForm(self.request.POST)
        if form.is_valid():
            form.save()
            messages.success(self.request, "thank you for subscribing our newsletter")
            return redirect("/")
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
