from django.db import models
from product.models import Payment
from django.contrib.auth.models import User

class Subscription(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    payment = models.ForeignKey(Payment, on_delete=models.CASCADE, null=True)
    subscription_name = models.CharField(max_length=20)
    subscription_amount = models.FloatField()
    subscribed_date = models.DateField(auto_now_add=True)
    expiry = models.DateField()
    status = models.BooleanField(default=False)

class SubscriptionNewsletter(models.Model):
    email = models.EmailField(max_length=50)

    def __str__(self):
        return self.email
