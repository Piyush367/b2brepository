from django.urls import path
from .import views

app_name = "subsctiption"
urlpatterns = [
    path('paid-service/', views.PaidServiceView.as_view(), name="paid_service"),
    path('', views.SubscriptionCheckoutView.as_view(), name="subscription_checkout"),
    path('newsletter/', views.SubscriptionNewsletterView.as_view(), name="subscription_newsletter"),
    path("handlerequest/", views.handlerequest, name="HandleRequest"),
     path("back/", views.back, name="back"),


]
