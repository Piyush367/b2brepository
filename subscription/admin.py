from django.contrib import admin
from .models import Subscription, SubscriptionNewsletter

@admin.register(Subscription)
class SubscriptionAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'subscription_name', 'subscription_amount', 'subscribed_date', 'expiry', 'status')

@admin.register(SubscriptionNewsletter)
class SubscriptionNewsletterAdmin(admin.ModelAdmin):
    list_display = ('id', 'email')