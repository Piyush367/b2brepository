from django import template
from accounts.models import SearchEngine


register = template.Library()


@register.simple_tag
def get_meta_title(request, slug):
    try:
        slug = slug[1:]
        s1 = slug.replace('/', ' ')
        list1 = s1.split()
        product = SearchEngine.objects.filter(user__account__slug__in = list1)[0]
        return product.meta_title
    except:
        return "Ramagyamart"


@register.simple_tag
def get_meta_decscription(request, slug):
    try:
        slug = slug[1:]
        s1 = slug.replace('/', ' ')
        list1 = s1.split()
        product = SearchEngine.objects.filter(user__account__slug__in = list1)[0]
        return product.meta_decscription
    except:
        return "b2bmart by Ramagyamart"

@register.simple_tag
def get_meta_keywords(request, slug):
    try:
        slug = slug[1:]
        s1 = slug.replace('/', ' ')
        list1 = s1.split()
        product = SearchEngine.objects.filter(user__account__slug__in = list1)[0]
        return product.meta_keywords
    except:
        return "Ramagyamart"
