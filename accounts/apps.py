from django.apps import AppConfig
from django import template
register = template.Library() 

@register.filter(name='has_type') 
def has_type(user, group_name):
    return user.Account.filter(business_types=group_name).exists() 

class AccountsConfig(AppConfig):
    name = 'accounts'

    def ready(self):
        import accounts.signals


