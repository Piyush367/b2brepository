from django.contrib import admin
from .models import Account

@admin.register(Account)
class AccountProductAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'mobile', 'state', 'pincode', 'company_name','slug', 'business_type', 'nature_of_business', 'otp_verified', 'gender', 'about_me')






