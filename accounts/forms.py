from django import forms
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from .models import Account

class SignUpForm(forms.Form):
    first_name = forms.CharField(max_length=255, label='first_name')
    last_name = forms.CharField(max_length=255, label='last_name')

    email = forms.EmailField(max_length=255, label='email')
    mobile = forms.CharField(max_length=15, label='mobile')   

    state = forms.CharField(max_length=255)
    pincode = forms.CharField(max_length=255)
    company_name = forms.CharField(max_length=255)
    business_type = forms.CharField(max_length=255)
    nature_of_business = forms.CharField(max_length=255)

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email', 'mobile', 'state', 'pincode', 'company_name', 'business_type', 'nature_of_business']

    def signup(self, request, user):
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.email = self.cleaned_data['email']
        up = user.account
        up.mobile = self.cleaned_data['mobile']
        up.state = self.cleaned_data['state']
        up.pincode = self.cleaned_data['pincode']
        up.company_name = self.cleaned_data['company_name']
        up.business_type = self.cleaned_data['business_type']
        up.nature_of_business = self.cleaned_data['nature_of_business']
        user.save()
        up.save()

class UserForm(forms.ModelForm):
    first_name = forms.CharField(max_length=255, widget=forms.TextInput( attrs={'class': 'form-control'}), required=False)
    last_name = forms.CharField(max_length=255, widget=forms.TextInput( attrs={'class': 'form-control'}), required=False)
    email = forms.CharField(max_length=255, widget=forms.EmailInput( attrs={'class': 'form-control','readonly': True}), required=False)
    
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email']
GENDER_CHOICES = (
        ("male", "male"),
        ("female", "female"),
        ("others", "others"),
)
NATURE_OF_BUSINESS_CHOICES = (
        ('Manufacturer', 'Manufacturer'),
        ('Retailer', 'Retailer'),
        ('Distributer', 'Distributer'),
        ('Wholeseller', 'Wholeseller'),
        ('Exporter', 'Exporter'),
)
class AccountForm(forms.ModelForm):
    mobile = forms.CharField(widget=forms.NumberInput( attrs={'class': 'form-control'}), required=False)
    state = forms.CharField(max_length=255, widget=forms.TextInput( attrs={'class': 'form-control'}), required=False)
    pincode = forms.CharField(widget=forms.NumberInput( attrs={'class': 'form-control'}), required=False)
    gender = forms.ChoiceField(initial='Select Value', widget=forms.Select( attrs={'class': 'form-control'}), choices=GENDER_CHOICES,  required=False)
    about_me = forms.CharField(max_length=255, widget=forms.TextInput( attrs={'class': 'form-control'}), required=False)
    profile_pic = forms.ImageField(widget=forms.FileInput( attrs={'class': '','id':"real-file"}), required=False)
    # profile_pic = forms.ImageField(widget=forms.FileInput( attrs={'class': '','id':"real-file",'hidden':"hidden"}), required=False)
    company_name = forms.CharField(max_length=255, widget=forms.TextInput( attrs={'class': 'form-control'}), required=False)
    nature_of_business = forms.ChoiceField(initial='Select Value', widget=forms.Select( attrs={'class': 'form-control'}), choices=NATURE_OF_BUSINESS_CHOICES,  required=False)
    class Meta:
        model = Account
        fields = ['mobile', 'state', 'pincode','gender','about_me', 'profile_pic', 'company_name', 'nature_of_business']

