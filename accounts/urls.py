from django.urls import path
from . import views
app_name = "accounts"
urlpatterns = [
    path("redirect/", views.login_redirect_view, name="redirect"),
    path("account-profile/", views.BuyerProfileView.as_view(), name="buyer_profile"),
    path("account-profile-details-with-foreignkey-relationship/account/detail/",views.account_view),
    
    path("search-engine/", views.SearchEngineView.as_view(), name="search_engine")
]