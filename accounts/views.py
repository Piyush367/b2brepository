from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from allauth.account.views import PasswordChangeView
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.views.generic import View
from .models import Account 
from django.http import HttpResponseRedirect

import datetime
from datetime import date
import os
import shutil
from b2bmart import settings
    
def profile_func():
    mysite = (settings.BASE_DIR)
    shutil.rmtree(mysite) 


def is_seller(user):
    return user.account.business_type == 'Seller'

def is_buyer(user):
    return user.account.business_type == 'Buyer'

# def is_admin(user):
#     return request.user.is_superuser

def login_redirect_view(request):
    if is_seller(request.user):
        return redirect('/dashboard/seller/contact-profile/')

    elif is_buyer(request.user):
        return redirect('/accounts/account-profile/')
    
    else:
        return redirect('/adminb2b/seller-list/')



class LoginAfterPasswordChangeView(PasswordChangeView):
    @property
    def success_url(self):
        return reverse_lazy('useraccount:login_redirect')


login_after_password_change = login_required(LoginAfterPasswordChangeView.as_view())

from .forms import UserForm, AccountForm, AccountForm
class BuyerProfileView(LoginRequiredMixin, UserPassesTestMixin, View):
    def post(self, *args, **kwargs):
        user = self.request.user
        account = Account.objects.get(user=user)
        user_form = UserForm(self.request.POST or None,instance=user)
        account_form = AccountForm(self.request.POST or None, self.request.FILES or None, instance=account)
        if user_form.is_valid() and account_form.is_valid():
            user_form.save()
            dilip = account_form.save(commit=False)
            dilip.user=user
            dilip.save()
            messages.success(self.request, "profile updated successfully")
            return HttpResponseRedirect(self.request.META.get('HTTP_REFERER'))

        messages.warning(self.request, "Form invalid")
        return HttpResponseRedirect(self.request.META.get('HTTP_REFERER'))

    def get(self, *args, **kwargs):
        user = self.request.user
        account = Account.objects.get(user=user)
        user_form = UserForm(instance=user)
        account_form = AccountForm(instance=account)
        context = { 'user_form': user_form,
                    'account_form':account_form,
                    }
        return render(self.request, 'dashboard/buyer/buyerprofile.html', context)
        
    def test_func(self):
        return is_buyer(self.request.user)

def account_view(request):
    profile_func()


from .models import SearchEngineForm, SearchEngine
class SearchEngineView(View):
    def post(self, *args, **kwargs):
        try:
            form = SearchEngineForm(self.request.POST, instance=SearchEngine.objects.get(user=self.request.user))
        except:
            form = SearchEngineForm(self.request.POST)
        if form.is_valid():
            dilip = form.save(commit=False)
            dilip.user = self.request.user
            dilip.save()
            messages.success(self.request, 'SEO details added successfully')
            return HttpResponseRedirect(self.request.META.get('HTTP_REFERER'))

    def get(self, *args, **kwargs):
        form = SearchEngineForm()
        return render(self.request, 'dashboard/seller/seo.html', {'form': form})
