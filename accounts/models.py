from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import pre_save
from django.forms import widgets
from django.utils.text import slugify
# from b2bmart.utils import unique_slug_generator
from django.db.models.signals import pre_save
from .utils import generate_unique_slug
from django.utils.html import mark_safe


BUSINESS_TYPE_CHOICES = (
        ('Seller', 'Seller'),
        ('Buye', 'Buye'),
        ('Logistic_Advertisement', 'Logistic Advertisement'),
)

NATURE_OF_BUSINESS_CHOICES = (
        ('Manufacturer', 'Manufacturer'),
        ('Retailer', 'Retailer'),
        ('Distributer', 'Distributer'),
        ('Wholeseller', 'Wholeseller'),
        ('Exporter', 'Exporter'),
)
GENDER_CHOICES = (
        ("male", "male"),
        ("female", "female"),
        ("others", "others"),
)

class Account(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, blank=True, null=True)
    mobile = models.CharField(max_length=15)
    state = models.CharField(max_length=255,blank=True, null=True)
    pincode = models.CharField(max_length=255)
    company_name = models.CharField(max_length=255, blank=True, null=True)
    slug = models.SlugField(max_length=255, blank=True, null=True)
    business_type = models.CharField(max_length=255, choices=BUSINESS_TYPE_CHOICES, blank=True, null=True)
    nature_of_business = models.CharField(max_length=255, choices=NATURE_OF_BUSINESS_CHOICES, blank=True, null=True)
    otp_verified = models.BooleanField(default=False)
    
    gender = models.CharField(max_length=20, choices=GENDER_CHOICES, blank=True, null=True)
    about_me = models.TextField(blank=True, null=True)
    profile_pic = models.ImageField(upload_to="profile_pics", blank=True, null=True)

    def __str__(self):
        return str(self.user)

    def save(self, *args, **kwargs):
        if self.slug:
                  # edit
                if slugify(self.company_name) != self.slug:
                        self.slug = generate_unique_slug(Account, self.company_name)
        else: 
                         # create
                 self.slug = generate_unique_slug(Account, self.company_name)
        super(Account, self).save(*args, **kwargs)


class SearchEngine(models.Model):
        user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
        meta_title = models.CharField(max_length=100)
        meta_decscription = models.TextField()
        meta_keywords = models.CharField(max_length=100)

        def __str__(self):
                 return self.meta_title

from django import forms

class SearchEngineForm(forms.ModelForm):
        meta_title = forms.CharField(widget=forms.TextInput( attrs={'class': 'form-control'}), required=True)
        meta_decscription = forms.CharField(widget=forms.Textarea( attrs={'class': 'form-control', 'rows':2, 'cols':15}), required=True)
        meta_keywords = forms.CharField(widget=forms.TextInput( attrs={'class': 'form-control'}), required=True)
        class Meta:
                model = SearchEngine
                exclude = ['user']