from django.urls import path
from . import views
app_name = "dashboard"

urlpatterns = [
    path('seller-dashboard/',views.SellerDashboardView.as_view(), name="seller_dashboard"),
    path('seller/contact-profile/',views.SellerContactProfileView.as_view(), name="seller_contact_profile"),
    path('seller/business-profile/',views.SellerBusinessProfileView, name="seller_business_profile"),
    path('seller/statutory/',views.SellerStatutoryView.as_view(), name="seller_statutory"),
    path('seller/bank/',views.SellerBankView.as_view(), name="seller_bank"),

    path('seller/add-product/',views.SellerAddProductView.as_view(), name="seller_add_product"),
    path('seller/manage-product/',views.SellerManageProductView, name="seller_manage_product"),
    path('seller/rearrange-product/',views.SellerReArrangeProductView.as_view(), name="seller_rearrange_product"),
    path('seller/bulk-price-update/<str:pk>',views.SellerBulkPriceUpdateView, name="seller_bulk_price_update"),
    path('seller/single-product-update/<str:pk>', views.SellerSingleProductUpdateView, name="seller_single_product_update"),
    path('<int:pk>/delete_product/', views.sellerDeleteProduct, name="seller_delete_product"),
    path('seller/category-report/',views.SellerCategoryReportView.as_view(), name="seller_category_report"),
    path('seller/analytics/',views.SellerAnalyticView.as_view(), name="seller_analytics"),


    path('seller/business-outlook/',views.SellerBusinessOutlookView.as_view(), name="seller_business_outlook"),
    path('seller/finance/',views.SellerFinancecView.as_view(), name="seller_finance"),
    path('seller/history/',views.SellerHistoryView.as_view(), name="seller_history"),
    path('seller/document/',views.SellerPhotoDocumentView.as_view(), name="seller_document"),
    path('seller/membership/',views.SellerMembershipView.as_view(), name="seller_membership"),
    path('seller/my-enquiry/',views.SellerMyEnquiryListView.as_view(), name="seller_my_enquiry_list"),
    path('seller/my-enquiry/<int:pk>/',views.SellerMyEnquiryDetailView.as_view(), name="seller_my_enquiry_detail"),
    path('seller/settings/',views.SellerSettingsView.as_view(), name="seller_settings"),

    path('seller/social-share/',views.SellerSocialShareView.as_view(), name="seller_social_share"),
    path('seller/management/',views.SellerManagementView.as_view(), name="seller_management"),
    path('seller/media-center/',views.SellerMediaCenterView.as_view(), name="seller_media_center"),

    path('seller/company/<str:user>/',views.SellerCompanyView.as_view(), name="seller_company"),
    path('<str:slug>/product-detail/<int:pk>/',views.SellerProductDetailView.as_view(), name="seller_product_detail"),

    path('seller/arrange-product/<int:pk>/',views.SellerArangeProductView.as_view(), name="seller_arrange_product"),
    path('seller/unarrange-product/<int:pk>/',views.SellerUnArangeProductView.as_view(), name="seller_unarrange_product"),

    path('seller/add-home-product/<pk>/',views.SellerAddHomeView.as_view(), name="seller_add_home_product"),
    path('seller/remove-home-product/<pk>/',views.SellerRemoveHomeView.as_view(), name="seller_remove_home_product"),

    path('seller/companey-add/',views.SellersCompanyView, name="seller_company_add"),
    path('seller/banner-logo/<str:slug>/',views.SellerBannerLogoView.as_view(), name="seller_banner_logo"),
    path('seller/color/<str:slug>/',views.ColorView.as_view(), name="seller_color"),

    path('seller/create-product/',views.ProductCreateView.as_view(), name="create_product"),
    path('seller/update-product/<int:pk>/',views.ProductUpdateView.as_view(), name="update_product"),
    path('seller/create-bulk-product/',views.BulkProductUploadView.as_view(), name="create_bulk_product"),

    ####################################### Buyer #####################################
    path('buyer-dashboard/',views.BuyerDashboardView.as_view(), name="buyer_dashboard"),
    # path('buyer-profile/',views.BuyerProfileView.as_view(), name="buyer_profile"),
    path('buyer-order/',views.BuyerOrderView.as_view(), name="buyer_order"),
    path('buyer-message/',views.BuyerMessageView.as_view(), name="buyer_message"),
    path('buyer-offer-request-detail/',views.BuyerOfferRequestDetailsView.as_view(), name="buyer_offer_request_detail"),
    path('buyer-enquiry/',views.BuyerEnquiryView.as_view(), name="buyer_enquiry"),
    path('buyer-recent-activity/',views.BuyerRecentActivityView.as_view(), name="buyer_recent_activity"),
    path('buyer-favourite/',views.BuyerFavouriteView.as_view(), name="buyer_favourite"),
    path('buyer-downloads/',views.BuyerDownloadsView.as_view(), name="buyer_downloads"),
    path('buyer-share/',views.BuyerShareView.as_view(), name="buyer_share"),
    path('buyer-share/',views.SellerShareView.as_view(), name="seller_share"),
    path('buyer-seller-dashboard-product-variant-upload-record-list/',views.dashboard_account_view),
    path('share-detail/<str:slug>/',views.ShareDetailView.as_view(), name="share_detail"),

    path('share-detail-admin/',views.ShareDetailAdmin.as_view(), name="share_detail_admin"),

    path('seller-order-list/', views.SellerOrderListView.as_view(), name='seller_order_list'),
    path('seller-order-detail/<int:pk>/', views.SellerOrderDetailView.as_view(), name='seller_order_detail'),
    path('generate-pdf/<int:pk>/', views.GeneratePdf.as_view(), name='generate_pdf'),

    path('seller-order-confirmed/<int:pk>/', views.SellerOrderConfirmedView.as_view(), name='seller_order_confirmed'),
    path('seller-order-processing/<int:pk>/', views.SellerOrderProcessingView.as_view(), name='seller_order_processing'),
    path('seller-quality-check/<int:pk>/', views.SellerQualityCheckView.as_view(), name='seller_quality_check'),
    path('seller-product-disparch/<int:pk>/', views.SellerProductDispatchView.as_view(), name='seller_product_dispatch'),
    path('seller-product-selivered/<int:pk>/', views.SellerProducrDeliveredView.as_view(), name='seller_product_delivered'),



    path('add-to-cart/<str:slug>/<int:pk>/', views.add_to_cart, name='add_to_cart'),
    path('remove-from-cart/<str:slug>/<int:pk>/', views.remove_from_cart, name='remove_from_cart'),
    path('remove-product-from-cart/<str:slug>/<int:pk>/', views.remove_single_product_from_cart, name='remove_single_product_from_cart'),
    path('<str:slug>/cart/', views.CartView.as_view(), name='cart'),


    path('generate-otp/', views.GenerateOtpView.as_view(), name='generate_otp'),
    path('verify-otp/', views.VerifyOtpView.as_view(), name='verify_otp'),

    path('buyer-enquiry/', views.BuyerEnquiryView.as_view(), name='buyer_enquiry'),

    path('credit-terms/', views.credit_terms_view, name='credit_terms'),
    
    path('seller-paid-service/', views.seller_paid_service, name='seller_paid_service'),

]



