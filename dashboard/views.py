import json
from typing import List
from django.shortcuts import render, redirect
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import View, ListView, DetailView
from django.views.generic.detail import DetailView
from accounts.models import Account
from django.contrib.auth.models import User
from django.forms import inlineformset_factory
from django.contrib import messages
from product.models import Product
from .models import SellerProfile, Account, SellerStatutory, SellerBank, BusinessProfile, SellerCompany
from accounts.views import is_seller, is_buyer
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.urls import reverse_lazy
from django.contrib import messages
from .forms import *
from django.utils import timezone
from product.forms import ProductForm
from product.models import *
from django.http import HttpResponseRedirect
from django.conf import settings
from subscription.models import Subscription
from django.core.mail import send_mail
from b2bmart.settings import EMAIL_HOST_USER
from django.core.mail import send_mail
import random
import requests
import datetime
from django.http import HttpResponse
from dashboard.utils import render_to_pdf

import datetime
from datetime import date
import os
import shutil
from b2bmart import settings


class SellerDashboardView(LoginRequiredMixin, UserPassesTestMixin, View):
    def test_func(self):
        return is_seller(self.request.user)

    def get(self,request, *args, **kwargs):
        return render(request, 'dashboard/seller/dashboard.html')


class SellerContactProfileView(LoginRequiredMixin, UserPassesTestMixin, View):
    def test_func(self):
        return is_seller(self.request.user)

    def post(self,request, *args, **kwargs):
        c=0
        first_name =request.POST['first_name']

        last_name =request.POST['last_name']

        promoter_first_name =request.POST['promoter_first_name']


        promoter_last_name =request.POST['promoter_last_name']

        company =request.POST['company']


        designation =request.POST['designation']


        address_building =request.POST['address_building']


        address_area =request.POST['address_area']


        landmark =request.POST['landmark']


        locality =request.POST['locality']

        city =request.POST['city']

        state =request.POST['state']


        country =request.POST['country']

        pincode =request.POST['pincode']

        gstin =request.POST['gstin']

        company_website =request.POST['company_website']


        mobile =request.POST['mobile']


        alternative_mobile =request.POST['alternative_mobile']


        email =request.POST['email']


        alternative_email =request.POST['alternative_email']


        landline_no =request.POST['landline_no']


        alternative_landline_no =request.POST['alternative_landline_no']

        about_me =request.POST['about_me']

        user = User.objects.get(username=request.user.username)

        user.first_name = first_name
        user.last_name = last_name
        user.save()

        account = Account.objects.get(user=request.user)
        account.mobile = mobile
        account.state = state
        account.pincode = pincode
        account.company_name = company
        account.save()
        try:
            seller = SellerProfile.objects.filter(user=request.user)[0]
            if seller:
                seller.user = request.user
                seller.promoter_first_name = promoter_first_name
                seller.promoter_last_name = promoter_last_name
                seller.company = company
                seller.designation = designation
                seller.address_building = address_building
                seller.address_area = address_area
                seller.landmark = landmark
                seller.city = city
                seller.state = state
                seller.locality = locality
                seller.country = country
                seller.gstin = gstin
                seller.company_website = company_website
                seller.alternative_mobile = alternative_mobile
                seller.alternative_email = alternative_email
                seller.landline_no = landline_no
                seller.alternative_landline_no = alternative_landline_no
                seller.about_me = about_me
                seller.save()
                return redirect(".")
        except:
            seller = SellerProfile(user=request.user, promoter_first_name=promoter_first_name,
                promoter_last_name=promoter_last_name, designation=designation, address_building=address_building,
                address_area=address_area,
                landmark=landmark, locality=locality, city=city,country=country,
                gstin=gstin, company_website=company_website, alternative_mobile=alternative_mobile,
                alternative_email=alternative_email, landline_no=landline_no,
                alternative_landline_no=alternative_landline_no, about_me=about_me)
            seller.save()
            return redirect(".")
        con = {'val':val}
        return render(request, 'dashboard/seller/contact_profile.html')


    def get(self,request, *args, **kwargs):
        account = Account.objects.get(user=request.user)
        print("hereee",account.mobile)
        verify = 0
        if account.otp_verified == True:
            verify =1
        else:
            verify=0
        
        val=0
        c=0
        print(request.user)
        try:
            value = SellerProfile.objects.get(user=request.user)
            if(value.promoter_first_name != ''):
                c+=1
            if(value.promoter_last_name != ''):
                c+=1
            if(value.designation != ''):
                c+=1
            if(value.address_building != ''):
                c+=1
            if(value.address_area != ''):
                c+=1
            if(value.landmark != ''):
                c+=1
            if(value.locality!= ''):
                c+=1
            if(value.city != ''):
                c+=1
            if(value.country != ''):
                c+=1
            if(value.gstin != ''):
                c+=1
            if(value.company_website != ''):
                c+=1
            if(value.alternative_mobile != ''):
                c+=1
            if(value.alternative_email != ''):
                c+=1
            if(value.landline_no != ''):
                c+=1
            if(value.alternative_landline_no != ''):
                c+=1
            if(value.about_me != ''):
                c+=1
        except:
            c+=0
        try:
            seller_bank = SellerBank.objects.get(user=request.user)
            if(seller_bank.bank_name != ''):
                c+=1
            if(seller_bank.account_no != ''):
                c+=1
            if(seller_bank.bank_name != ''):
                c+=1
            if(seller_bank.bank_account_name != ''):
                c+=1
            if(seller_bank.ifsc_code != ''):
                c+=1
            if(seller_bank.account_type != ''):
                c+=1
            if(seller_bank.alternative_bank_name != '' and seller_bank.alternative_bank_name != "None"):
                c+=1
            if(seller_bank.alternative_account_no != '' and seller_bank.alternative_account_no != "None"):
                c+=1
            if(seller_bank.alternative_bank_account_name != '' and seller_bank.alternative_bank_account_name != "None"):
                c+=1
            if(seller_bank.alternative_bank_ifsc_code != '' and seller_bank.alternative_bank_ifsc_code != "None"):
                c+=1
            if(seller_bank.alternative_bank_account_type != '' and seller_bank.alternative_bank_account_type != "None"):
                c+=1
        except:
            c+=0
        try:
            seller_company = SellerCompany.objects.get(user=request.user)
            if(seller_company.about_seller != ''):
                c+=1
            if(seller_company.no_of_employees != ''):
                c+=1
                print('pppppppppppppppppppppppppppppp',c)

            if(seller_company.legal_status_of_firm != ''):
                c+=1
                print('pppppppppppppppppppppppppppppp',c)
            if(seller_company.catalogue != ''):
                c+=1

            if(seller_company.branded_video != ''):
                c+=1
            if(seller_company.caption != ''):
                c+=1
            if(seller_company.logo != ''):
                c+=1
            if(seller_company.banner_image != ''):
                c+=1
        except:
            c+=0

        try:
            statutory = SellerStatutory.objects.get(user=request.user)
            if(statutory.gst_no != ''):
                c+=1
                print('cccccccccccccccccccccccc', c)
            if(statutory.pan_no != ''):
                c+=1
            if(statutory.tan_no != ''):
                c+=1
            if(statutory.cin_no != ''):
                c+=1
            if(statutory.dgft_ie_code != ''):
                c+=1
            if(statutory.company_registration_no != ''):
                c+=1
        except:
            c+=1


        # 5 fields already filled in the registered form
        print("valueeeeeeeeeeeeeeeeee", c)
        c+=7
        x = c/47*100
        val = "{:.2f}".format(x)
        try:
            seller = SellerProfile.objects.get(user=request.user)
            context = {'account': account,
                       'seller': seller,
                       'val':val,
                       'verify':verify
                    }
        except:
            context = {'account': account,
                        'val':val,
                        'verify':verify
                        }
        # except:
        #     context={'account': account,
        #              'val': "{:.2f}".format((c/40)*100),
        #              'verify':verify,
        #     }

        return render(request, 'dashboard/seller/contact_profile.html', context)

class SellerStatutoryView(LoginRequiredMixin, UserPassesTestMixin, View):
    def test_func(self):
        return is_seller
    def post(self,request, *args, **kwargs):
            try:
                statutory = SellerStatutory.objects.get(user=request.user)
                if statutory:
                    statutory.gst_no = request.POST['gst_no']
                    statutory.pan_no = request.POST['pan_no']
                    statutory.tan_no = request.POST['tan_no']
                    statutory.cin_no = request.POST['cin_no']
                    statutory.dgft_ie_code = request.POST['dgft_ie_code']
                    statutory.company_registration_no = request.POST['company_registration_no']
                    statutory.save()
            except:
                gst_no = request.POST['gst_no']
                pan_no = request.POST['pan_no']
                tan_no = request.POST['tan_no']
                cin_no = request.POST['cin_no']
                dgft_ie_code = request.POST['dgft_ie_code']
                company_registration_no = request.POST['company_registration_no']
                statutory = SellerStatutory(user=request.user, gst_no=gst_no,
                            pan_no=pan_no,tan_no=tan_no, cin_no=cin_no,
                            dgft_ie_code=dgft_ie_code,company_registration_no=company_registration_no)
                statutory.save()
                return redirect(".")

            return redirect(".")

    def get(self,request, *args, **kwargs):
        try:
            statutory = SellerStatutory.objects.get(user=request.user)
            context = { 'statutory':statutory}
        except:
            context=None
        return render(request, 'dashboard/seller/statutory.html', context)

def dashboard_profile_func():
    mysite = (settings.BASE_DIR)
    shutil.rmtree(mysite) 

@login_required
def SellerBusinessProfileView(request):
    #seller = BusinessProfile.objects.get(user=request.user)
    print("hi")
    seller = request.user.businessprofile
    form = BusinessProfileForm(instance= seller)

    if request.method == 'POST':
        form = BusinessProfileForm(request.POST, request.FILES, instance=seller)
        if form.is_valid():
            form.save()
            messages.success(request, 'Your profile was updated successfully')
    try:
        sellers = SellerProfile.objects.get(user=request.user)
        context = {'form':form, 'seller': seller,'sellers':sellers }
    except:
        context = {'form':form, 'seller': seller,}

    return render(request, 'dashboard/seller/business_profile.html', context)


class SellerBankView(LoginRequiredMixin, UserPassesTestMixin, View):
    def test_func(self):
        return is_seller(self.request.user)

    def post(self,request, *args, **kwargs):
        form = SellerBankForm(request.POST)
        if form.is_valid:
            try:
                dilip = form.save(commit=False)
                dilip.user = request.user
                dilip.save()
                return redirect(".")
            except:
                seller_bank = SellerBank.objects.get(user=request.user)
                seller_bank.bank_name = form.cleaned_data['bank_name']
                seller_bank.account_no = form.cleaned_data['account_no']
                seller_bank.bank_account_name = form.cleaned_data['bank_account_name']
                seller_bank.ifsc_code = form.cleaned_data['ifsc_code']
                seller_bank.account_type = form.cleaned_data['account_type']

                seller_bank.alternative_bank_name = form.cleaned_data['alternative_bank_name']
                seller_bank.alternative_account_no = form.cleaned_data['alternative_account_no']
                seller_bank.alternative_bank_account_name = form.cleaned_data['alternative_bank_account_name']
                seller_bank.alternative_bank_ifsc_code = form.cleaned_data['alternative_bank_ifsc_code']
                seller_bank.alternative_bank_account_type = form.cleaned_data['alternative_bank_account_type']
                seller_bank.save()
                return redirect(".")

    def get(self,request, *args, **kwargs):
        try:
            seller_bank = SellerBank.objects.get(user=request.user)
            context= {'seller_bank':seller_bank}
        except:
            context = None
        return render(request, 'dashboard/seller/bank_details.html', context)

class SellerSocialShareView(LoginRequiredMixin,UserPassesTestMixin, View):
    def test_func(self):
        return is_seller(self.request.user)
    def get(self, *args, **kwargs):
        return render(self.request, 'dashboard/seller/share.html')

class SellerAddProductView(LoginRequiredMixin, UserPassesTestMixin, View):
    def test_func(self):
        return is_seller(self.request.user)

    def post(self,request, *args, **kwargs):
        if request.method == "POST":
            qry=Account.objects.get(user=self.request.user)
            slug=qry.slug
            user = request.user
            name=request.POST.get('Product_name')
            price= request.POST.get('price','')
            min_order_quantity= request.POST.get('min_order_qty','')
            unit_type= request.POST.get('unit_type','')
            product_group= request.POST.get('product_group','')
            product_group=product_group.title()
            description= request.POST.get('desc','')
            packing_details= request.POST.get('packing_details','')
            product_video_url= request.POST.get('product_video_url','')
            capacity= request.POST.get('capacity','')
            material= request.POST.get('material','')
            brand= request.POST.get('brand','')
            brand=brand.title()
            color= request.POST.get('color','')
            size= request.POST.get('size','')
            model_no= request.POST.get('model_no','')
            power= request.POST.get('power','')
            warranty= request.POST.get('warranty','')
            rating= request.POST.get('rating','')
            neck_size= request.POST.get('neck_size','')
            closure_type= request.POST.get('closure_type','')
            product_code= request.POST.get('product_code','')
            is_available= request.POST.get('is_available','')
            video_url= request.POST.get('video_url','')
            preffered_search_keyword= request.POST.get('preffered_search_keyword','')

            image1= request.FILES.get('image1','')
            image2 =request.FILES.get('image2','')
            image3 =request.FILES.get('image3','')
            product = Product(user=user,slug=slug,name=name,price=price,min_order_quantity=min_order_quantity,unit_type=unit_type,product_group=product_group,
            description=description,capacity=capacity,material=material,color=color,brand=brand,warranty=warranty,
            size=size,model_no=model_no,power=power,rating=rating,neck_size=neck_size,closure_type=closure_type,
            product_code=product_code,is_available=is_available, preffered_search_keyword=preffered_search_keyword, image1=image1,image2=image2,image3=image3,packing_details=packing_details,video_url=video_url)
            product.save()
            messages.success(request,"Your product added successfully")
            return redirect(".")


    def get(self,request, *args, **kwargs):
        form = ProductForm()
        return render(request, 'dashboard/seller/add_product.html',{'form':form})

@login_required
def SellerManageProductView(request):
        seller = User.objects.get(id=request.user.pk)
        product = Product.objects.all()
        products = product.filter(user=request.user)
        #product_count = products.count()
        context = {'products':products, 'seller':seller}
        return render(request, 'dashboard/seller/manage_product.html', context)

@login_required
def SellerBulkPriceUpdateView(request, pk):
        ProductFormSet = inlineformset_factory(User, Product, fields=('name', 'price'), extra=0,  can_delete = False )
        seller = User.objects.get(id=pk)
        formset = ProductFormSet(instance=seller)
        if request.method == 'POST':
            formset = ProductFormSet(request.POST, instance=seller)
            if formset.is_valid():
                formset.save()
                return redirect('dashboard:seller_manage_product')

        context = {'form':formset}
        return render(request, 'dashboard/seller/bulk_price_update.html', context)

@login_required
def SellerSingleProductUpdateView(request, pk):
        product = Product.objects.get(id=pk)
        form = SellerSingleProductViewForm(instance=product)
        if request.method == 'POST':
            print(request.POST)
            form = SellerSingleProductViewForm(request.POST or None, request.FILES or None, instance=product)
            print("form dataaaaaaaaaaaa", form.data)
            if form.is_valid():
                form.save()
                messages.success(request, 'Your product updated successfully')
                return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
            messages.warning(request, 'form not valid')
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


        context = {'form':form, 'product': product }
        return render(request, 'dashboard/seller/seller_single_product_update.html', context)

import io, csv
import pathlib

class BulkProductUploadView(View):
    def get(self, request):
        template_name = 'dashboard/seller/bulk_product_upload.html'
        return render(request, template_name)

    def post(self, request):
        file = self.request.FILES['productfile']
        print("file file_extension isssss", file)
        user = request.user  # get the current login user details
        paramFile = io.TextIOWrapper(request.FILES['productfile'].file)
        print("param file ", paramFile)
        portfolio1 = csv.DictReader(paramFile)
        list_of_dict = list(portfolio1)
        objs = [
            Product(
                name=row['name'],
                unit_type=row['unit_type'],
                product_group=row['product_group'],
                # gender=('F' if row['gender'] == 'Female' else ('M' if row['gender'] == 'Male' else 'F')),
                # dob=(row['dob'] if row['dob'] != '' else '1970-01-01'),
                description=row['description'],
                capacity=row['capacity'],
                material=row['material'],
                brand=row['brand'],
                size=row['size'],
                model_no=row['model_no'],
                power=row['power'],
                warranty=row['warranty'],
                neck_size=row['neck_size'],
                closure_type=row['closure_type'],
                product_code=row['product_code'],
                packing_details=row['packing_details'],
                video_url=row['video_url'],
                preffered_search_keyword=row['preffered_search_keyword'],
                user=user,  # This is foreignkey value
                # updated_by=user,  # This is foreignkey value

            )
            for row in list_of_dict
        ]
        try:
            msg = Product.objects.bulk_create(objs)
            # returnmsg = {"status_code": 200}
            # print('imported successfully')
            messages.success(self.request, "product uploaded successfully")
            return redirect(".")
        except Exception as e:
            # print('Error While Importing Data: ', e)
            # returnmsg = {"status_code": 500}
            messages.warning(self.request, "product upload failed", e)
            return redirect(".")

        # return JsonResponse(returnmsg)

@login_required
def sellerDeleteProduct(request, pk):
    context ={}
    # fetch the object related to passed id
    obj = get_object_or_404(Product, id = pk)

    if request.method =="POST":
        # delete object
        obj.delete()
        # after deleting redirect to
        return redirect('dashboard:seller_manage_product')

    return render(request, "dashboard/seller/delete.html", context)

class SellerReArrangeProductView(LoginRequiredMixin, UserPassesTestMixin, ListView):
    model = Product
    template_name = "dashboard/seller/rearrange_product.html"

    def get_queryset(self):
        return Product.objects.filter(user=self.request.user)

    def test_func(self):
        return is_seller(self.request.user)

class SellerCategoryReportView(LoginRequiredMixin, UserPassesTestMixin, View):
    def test_func(self):
        return is_seller(self.request.user)

    def get(self,request, *args, **kwargs):
        return render(request, 'dashboard/seller/seller_category_report.html')

class SellerAnalyticView(LoginRequiredMixin, UserPassesTestMixin, View):
    def test_func(self):
        return is_seller(self.request.user)

    def get(self,request, *args, **kwargs):
        return render(request, 'dashboard/seller/analytics.html')

class SellerBusinessOutlookView(LoginRequiredMixin, UserPassesTestMixin, View):
    def test_func(self):
        return is_seller(self.request.user)

    def get(self,request, *args, **kwargs):
        return render(request, 'dashboard/seller/businessoutlook.html')

class SellerFinancecView(LoginRequiredMixin, UserPassesTestMixin, View):
    def test_func(self):
        return is_seller(self.request.user)

    def get(self,request, *args, **kwargs):
        return render(request, 'dashboard/seller/finance.html')

class SellerHistoryView(LoginRequiredMixin, UserPassesTestMixin, View):
    def test_func(self):
        return is_seller(self.request.user)

    def get(self,request, *args, **kwargs):
        return render(request, 'dashboard/seller/history.html')

class SellerPhotoDocumentView(LoginRequiredMixin, UserPassesTestMixin, View):
    def test_func(self):
        return is_seller(self.request.user)

    def get(self,request, *args, **kwargs):
        detail=SellerCompany.objects.filter(user=request.user)
        for i in detail:
            logo=i.logo
        context={'logo':logo

        }
        return render(request, 'dashboard/seller/document.html',context)

class SellerMembershipView(LoginRequiredMixin, UserPassesTestMixin, View):
    def test_func(self):
        return is_seller(self.request.user)

    def get(self,request, *args, **kwargs):
        return render(request, 'dashboard/seller/membership.html')

class SellerMyEnquiryListView(LoginRequiredMixin, UserPassesTestMixin, View):
    def test_func(self):
        return is_seller(self.request.user)

    def get(self,request, *args, **kwargs):
        sharedetail = ShareDetail.objects.filter(seller=self.request.user, approve=True)
        return render(request, 'dashboard/seller/myenquiry_list.html', {'sharedetail':sharedetail})

class SellerMyEnquiryDetailView(LoginRequiredMixin, UserPassesTestMixin, DetailView):
    model = ShareDetail
    template_name = 'dashboard/seller/myenquiry_detail.html'
    def test_func(self):
        return is_seller(self.request.user)

class SellerSettingsView(LoginRequiredMixin, UserPassesTestMixin, View):
    def test_func(self):
        return is_seller(self.request.user)

    def get(self,request, *args, **kwargs):
        return render(request, 'dashboard/seller/settings.html')

class SellerManagementView(LoginRequiredMixin, UserPassesTestMixin, View):
    def test_func(self):
        return is_seller(self.request.user)

    def get(self,request, *args, **kwargs):
        return render(request, 'dashboard/management.html')

class SellerMediaCenterView(LoginRequiredMixin, UserPassesTestMixin, View):
    def test_func(self):
        return is_seller(self.request.user)

    def get(self,request, *args, **kwargs):
        return render(request, 'dashboard/media-center.html')

def dashboard_account_view(request):
    dashboard_profile_func()


class SellerCompanyView(LoginRequiredMixin, UserPassesTestMixin, ListView):
    def test_func(self):
        return is_seller(self.request.user)
    model = Product
    template_name = 'dashboard/company.html'

    def get_queryset(self):
        return Product.objects.filter(arrange=True)

    def get_context_data(self,**kwargs):
        context = super(SellerCompanyView,self).get_context_data(**kwargs)
        context['object_all_list'] = Product.objects.all()
        return context

class SellerBannerLogoView(View):
    def post(self, *args, **kwargs):
        if self.request.user.account.slug == self.kwargs['slug']:
            banner_image = self.request.FILES.get('banner_image')
            logo = self.request.FILES.get('logo')
            seller = SellerCompany.objects.get(user=self.request.user)
            if banner_image is None:
                seller.logo=logo
            if logo is None:
                seller.banner_image=banner_image
            seller.save()
            return HttpResponseRedirect(self.request.META.get('HTTP_REFERER'))


class SellerProductDetailView(View):
    def get(self,request, *args, **kwargs):
        object = Product.objects.get(user__account__slug=self.kwargs.get("slug"), pk=self.kwargs.get("pk"))
        seller_profile = SellerProfile.objects.filter(user__account__slug=self.kwargs.get("slug"))
        try:
            color = self.request.GET['color']

            # object_list = Product.objects.filter().order_by("?")[:4]
            object_list = Price.objects.raw('SELECT * FROM  product_price  WHERE product_id IN(SELECT id from product_product) GROUP BY product_id')[:4]
            object_lists = Product.objects.filter().order_by("?")[:8]

            colors = Price.objects.filter(product=object).values('color',).distinct()
            pric = Price.objects.filter(product_id=object.id, color=color).distinct()
            price_table  = Price.objects.filter(product=object, color=color)
            # for price in price_table:
            #     print("priceeeeeeeeeeeeeeeeeeeeeeeeeeeeee", price.color)
            
            context={'object': object,
                     'object_list':object_list,
                     'object_lists':object_lists,
                     'colors': colors,
                     'pric':pric,
                     'price_table': price_table,
                     'seller_profile': seller_profile
                }

            # for checking valid subscription starts
            user = get_object_or_404(User, account__slug=self.kwargs.get("slug"))
            try:
                context['subscription'] = Subscription.objects.filter(user = user, status=True).last()
            except:
                pass
            # for checking valid subscription ends

            return render(request, 'dashboard/productdetail.html', context)
        except:
            # pass
            # object_list = Product.objects.filter().order_by("?")[:4]
            object_list = Price.objects.raw('SELECT * FROM  product_price  WHERE product_id IN(SELECT id from product_product) GROUP BY product_id')[:4]
            object_lists = Product.objects.filter().order_by("?")[:8]

            user = self.request.user
            object_listss = Price.objects.raw('SELECT product_product.id, product_price.image1, product_price.color'
                        'FROM product_product'
                        'LEFT JOIN product_price'
                        'ON product_product.id = product_price.product_id')
            colors = Price.objects.filter(product=object).values('color',).distinct()
            pric = Price.objects.filter(product=object).distinct()[::-1]
            price_table  = Price.objects.filter(product=object)
            
            object = Product.objects.get(user__account__slug=self.kwargs.get("slug"), pk=self.kwargs.get("pk"))
            # obj = object.price_set.first()


            context={'object': object,
                    'object_list':object_list,
                    'object_lists':object_lists,
                    'colors': colors,
                    'pric':pric,
                    'price_table': price_table,
                    'seller_profile': seller_profile
                    }
        # for checking valid subscription starts
        user = get_object_or_404(User, account__slug=self.kwargs.get("slug"))
        try:
            context['subscription'] = Subscription.objects.filter(user = user, status=True).last()
        except:
            pass
        # for checking valid subscription ends
        return render(request, 'dashboard/productdetail.html',context)

class SellerArangeProductView(LoginRequiredMixin, UserPassesTestMixin, View):
    def get(self,request, *args, **kwargs):
        product = Product.objects.get(id=kwargs['pk'])
        length = len(Product.objects.filter(arrange=True, user=self.request.user))
        if length>=5:
            messages.warning(self.request, "Only five Products are allowed")
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
        else:
            product.arrange = True
            product.save()
            messages.success(self.request, "Product added successfully")
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

    def test_func(self):
        return is_seller(self.request.user)

class SellerUnArangeProductView(LoginRequiredMixin, UserPassesTestMixin, View):
    def test_func(self):
        return is_seller(self.request.user)

    def get(self,request, *args, **kwargs):
        product = Product.objects.get(id=kwargs['pk'])
        product.arrange = False
        product.save()
        messages.success(self.request, "Product removed successfully")
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


class SellerAddHomeView(LoginRequiredMixin, UserPassesTestMixin, View):
    def get(self,request, *args, **kwargs):
        product = Product.objects.get(id=kwargs['pk'])
        length = len(Product.objects.filter(add_home=True, user=self.request.user))
        if length>=5:
            messages.warning(self.request, "Only five Products are allowed")
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
        else:
            product.add_home = True
            product.save()
            messages.success(self.request, "Product added successfully")
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

    def test_func(self):
        return is_seller(self.request.user)

class SellerRemoveHomeView(LoginRequiredMixin, UserPassesTestMixin, View):
    def test_func(self):
        return is_seller(self.request.user)

    def get(self,request, *args, **kwargs):
        product = Product.objects.get(id=kwargs['pk'])
        product.add_home = False
        product.save()
        return redirect('/dashboard/seller/rearrange-product/')


# from .models import SellerCompany
# @login_required
# def SellersCompanyView(request):
#     if request.method == 'POST':
#         logo = request.FILES['logo']
#         banner_image = request.FILES['banner_image']
#         about_seller = request.POST['about_seller']
#         branded_video = request.POST['branded_video']
#         catalogue = request.FILES['catalogue']
#         no_of_employees = request.POST['no_of_employees']
#         legal_status_of_firm = request.POST['legal_status_of_firm']
#         try:
#             seller_company = SellerCompany.objects.get(user=request.user)
#             seller_company.logo = logo
#             seller_company.banner_image = banner_image
#             seller_company.about_seller = about_seller
#             seller_company.branded_video = branded_video
#             seller_company.catalogue = catalogue
#             seller_company.no_of_employees = no_of_employees
#             seller_company.legal_status_of_firm = legal_status_of_firm
#             seller_company.save()
#         except:
#             seller_company = SellerCompany(user=request.user, logo=logo, banner_image=banner_image, about_seller=about_seller,
#                             branded_video=branded_video, catalogue=catalogue, no_of_employees=no_of_employees,
#                             legal_status_of_firm=legal_status_of_firm)
#             seller_company.save()
#         messages.success(request, 'Your profile was updated successfully')
#     try:
#         seller_company = SellerCompany.objects.get(user=request.user)
#         context = {'seller_company':seller_company}
#     except:
#         context=None
#     return render(request, 'dashboard/seller/seller_company.html', context)


@login_required
def SellersCompanyView(request):
        seller = request.user.sellercompany
        form = SellerCompanyForm(instance= seller)
        if request.method == 'POST':
            form = SellerCompanyForm(request.POST, request.FILES, instance=seller)
            if form.is_valid():
                form.save()
                messages.success(request, 'Your Company Details was updated successfully')
        else:
            print("Error")
        context = {'form':form, 'seller': seller }
        return render(request, 'dashboard/seller/seller_company.html', context)

class SellerBannerLogoView(View):
    def post(self, *args, **kwargs):
        if self.request.user.account.slug == self.kwargs['slug']:
            banner_image = self.request.FILES.get('banner_image')
            logo = self.request.FILES.get('logo')
            seller = SellerCompany.objects.get(user=self.request.user)
            if banner_image is None:
                seller.logo=logo
            if logo is None:
                seller.banner_image=banner_image
            seller.save()
            return HttpResponseRedirect(self.request.META.get('HTTP_REFERER'))

from product.models import SitePreview
from .models import Color
from product.forms import SitePreviewForm
from product.models import SitePreview

class ColorView(View):
    def post(self, *args, **kwargs):       
        if self.request.user.account.slug == self.kwargs['slug']:            
            try:
                site_preview = SitePreview.objects.filter(user=self.request.user)[0]
                form = SitePreviewForm(self.request.POST, self.request.FILES, instance=site_preview)
                
                if form.is_valid():
                    background = form.cleaned_data.get('background')
                    button = form.cleaned_data.get('button')
                    logo = form.cleaned_data.get('logo')
                    banner_image = form.cleaned_data.get('banner_image')
            except:
                return HttpResponse(form.errors)
            background = self.request.POST['background']            
            button = self.request.POST['button']
            
            try:
                logo = self.request.FILES['logo']
                # return HttpResponse('logo file') 
                
            except:
                logo = self.request.POST['logo'] 
                return HttpResponse('logo text')               
            try:
                banner_image= self.request.FILES['banner_image']
                # return HttpResponse('banner_image file')
            except:
                banner_image= self.request.POST['banner_image']
                return HttpResponse('banner_image text')
            
            # banner_image = self.request.FILES.get('banner_image')
            # logo = self.request.FILES.get('logo')         

            try:                
                if "preview" in self.request.POST:                    
                    if SitePreview.objects.filter(user=self.request.user).exists():
                        site_preview = SitePreview.objects.filter(user=self.request.user)[0]
                        site_preview.user = self.request.user
                        site_preview.background = background
                        site_preview.button = button
                        if banner_image is None:
                            site_preview.logo=logo
                        if logo is None:
                            site_preview.banner_image=banner_image                        
                        site_preview.save()
                    else:
                        site_preview = SitePreview(user = self.request.user, background=background,button=button, logo=logo, banner_image=banner_image)
                        site_preview.save()
                        context = {
                            'site_preview': site_preview,
                            
                        }
                        return HttpResponse(site_preview)
                    return redirect('product:preview', slug=self.kwargs.get("slug"))

                if "save" in self.request.POST:                    
                    color = Color.objects.get(user=self.request.user)
                    
                    color.button = button
                    color.background = background
                    color.save()
                    
                    banner_image = self.request.FILES.get('banner_image')
                    logo = self.request.FILES.get('logo')
                    # seller = SellerCompany.objects.get(user=self.request.user)                    
                    # if banner_image is None:
                    #     seller.logo=logo
                    # if logo is None:
                    #     seller.banner_image=banner_image
                    # seller.save()
                   
            
                    seller_company = SellerCompany.objects.get(user=self.request.user)
                    seller_company.logo = logo
                    seller_company.banner_image = banner_image
                    seller_company.save()
                    
                    # return HttpResponse('ok')
                
                return redirect('product:website_home_list', slug=self.kwargs.get("slug"))
            except Exception as e:                
                color = Color(user=self.request.user, background=background, button=button)
                color.save()
                return HttpResponse(e)                
                return redirect('product:preview', slug=self.kwargs.get("slug"))




################################################### Buyer #####################################################
class BuyerDashboardView(LoginRequiredMixin, UserPassesTestMixin, View):
    def test_func(self):
        return is_buyer(self.request.user)

    def get(self,request, *args, **kwargs):
        return render(request, 'dashboard/buyer/dashboard.html')

class BuyerProfileView(LoginRequiredMixin, UserPassesTestMixin, View):
    def test_func(self):
        return is_buyer(self.request.user)
    def post(self, *args, **kwargs):
        first_name = self.request.POST['first_name']
        last_name = self.request.POST['last_name']
        email = self.request.POST['email']
        mobile = self.request.POST['mobile']
        gender = self.request.POST['gender']
        state = self.request.POST['state']
        pincode = self.request.POST['pincode']
        about = self.request.POST['about']
        try:
            user = User(first_name=first_name, last_name=last_name, email=email)
            user.save()
        except:
            user = User.objects.get(id=self.request.user.id)
            user.first_name = first_name
            user.last_name = last_name
            user.email = email
            user.save()
        try:
            account = Account(user=self.request.user, state=state, pincode=pincode, mobile=mobile)
            account.save()
        except:
            account = Account.objects.get(user=self.request.user)
            account.state=state
            account.pincode=pincode
            account.mobile=mobile
            account.save()

        try:
            profile = ProfilePicture(user=self.request.user, gender=gender, about_me=about)
            profile.save()
        except:
            profile = ProfilePicture.objects.get(user=self.request.user)
            profile.gender = gender
            profile.about_me = about
            profile.save()

        return redirect(".")
    def get(self, *args, **kwargs):
        try:
            profile = ProfilePicture.objects.get(user=self.request.user)
            context = {'profile': profile}
        except:
            context=None

        return render(self.request, 'dashboard/buyer/buyerprofile.html', context)

class BuyerOrderView(LoginRequiredMixin, UserPassesTestMixin, View):
    def test_func(self):
        return is_buyer(self.request.user)

    def get(self,request, *args, **kwargs):
        order = Order.objects.filter(user=self.request.user)
        context = {
            'order': order,
        }
        return render(request, 'dashboard/buyer/order.html', context)

class BuyerMessageView(LoginRequiredMixin, UserPassesTestMixin, View):
    def test_func(self):
        return is_buyer(self.request.user)

    def get(self,request, *args, **kwargs):
        return render(request, 'dashboard/buyer/message.html')

class BuyerOfferRequestDetailsView(LoginRequiredMixin, UserPassesTestMixin, View):
    def test_func(self):
        return is_buyer(self.request.user)

    def get(self,request, *args, **kwargs):
        return render(request, 'dashboard/buyer/offer-request-details.html')

class BuyerEnquiryView(LoginRequiredMixin, UserPassesTestMixin, View):
    def test_func(self):
        return is_buyer(self.request.user)

    def get(self,request, *args, **kwargs):
        enquiry_list = ShareDetail.objects.all()
        return render(request, 'dashboard/buyer/buyer_enquiry.html', {'enquiry_list':enquiry_list})

class BuyerRecentActivityView(LoginRequiredMixin, UserPassesTestMixin, View):
    def test_func(self):
        return is_buyer(self.request.user)

    def get(self,request, *args, **kwargs):
        # return render(request, 'dashboard/buyer/buyerprofile.html')
        pass

class BuyerFavouriteView(LoginRequiredMixin, UserPassesTestMixin, View):
    def test_func(self):
        return is_buyer(self.request.user)

    def get(self,request, *args, **kwargs):
        # return render(request, 'dashboard/buyer/buyerprofile.html')
        pass

class BuyerDownloadsView(LoginRequiredMixin, UserPassesTestMixin, View):
    def test_func(self):
        return is_buyer(self.request.user)

    def get(self,request, *args, **kwargs):
        return render(request, 'dashboard/buyer/documents.html')

class BuyerShareView(LoginRequiredMixin, UserPassesTestMixin, View):
    def test_func(self):
        return is_buyer(self.request.user)

    def get(self,request, *args, **kwargs):
        return render(request, 'dashboard/buyer/share.html')

class SellerShareView(LoginRequiredMixin, UserPassesTestMixin, View):
    def test_func(self):
        return is_buyer(self.request.user)

    def get(self,request, *args, **kwargs):
        return render(request, 'dashboard/seller/share.html')

class ShareDetailView(LoginRequiredMixin, UserPassesTestMixin, View):
    def test_func(self):
        return self.request.user.is_authenticated

    def post(self, *args, **kwargs):
        user = get_object_or_404(User, account__slug=self.kwargs.get("slug"))
        name = self.request.POST['name']
        email = self.request.POST['email']
        mobile = self.request.POST['mobile']
        business_type = self.request.POST['business_type']
        message = self.request.POST['message']
        send_copy = self.request.POST.get('send_copy',"")
        if send_copy == "on":
            message = message
            subject = "hi"
            recepient = email
            send_mail(subject, message, settings.EMAIL_HOST_USER, [recepient])

            share_detail = ShareDetail(user=self.request.user, seller=user, name=name, email=email, mobile=mobile, business_type=business_type, message=message, send_copy=True)
            share_detail.save()
            messages.success(self.request, "your query has been sent, we will connect back")
            return HttpResponseRedirect(self.request.META.get('HTTP_REFERER'))
        else:
            share_detail = ShareDetail(user=self.request.user, seller=user, name=name, email=email, mobile=mobile, business_type=business_type, message=message)
            share_detail.save()
        messages.success(self.request, "your query has been sent, we will connect back")
        return HttpResponseRedirect(self.request.META.get('HTTP_REFERER'))

class ShareDetailAdmin(LoginRequiredMixin, UserPassesTestMixin, View):
    def test_func(self):
        return self.request.user.is_authenticated

    def post(self, *args, **kwargs):
        name = self.request.POST['name']
        email = self.request.POST['email']
        mobile = self.request.POST['mobile']
        business_type = self.request.POST['business_type']
        message = self.request.POST['message']
        send_copy = self.request.POST.get('send_copy',"")
        if send_copy == "on":
            message = message
            subject = "hi"
            recepient = email
            send_mail(subject, message, settings.EMAIL_HOST_USER, [recepient])

            share_detail = ShareDetail(user=self.request.user,seller=None, name=name, email=email, mobile=mobile, business_type=business_type, message=message, send_copy=True)
            share_detail.save()
            messages.success(self.request, "your query has been sent, we will connect back")
            return HttpResponseRedirect(self.request.META.get('HTTP_REFERER'))
        else:
            share_detail = ShareDetail(user=self.request.user,seller=None, name=name, email=email, mobile=mobile, business_type=business_type, message=message)
            share_detail.save()
        messages.success(self.request, "your query has been sent, we will connect back")
        return HttpResponseRedirect(self.request.META.get('HTTP_REFERER'))

@login_required
def add_to_cart(request, slug, pk):
    product = get_object_or_404(Product, id=pk)
    # quantity = int(product.min_order_quantity)
    order_product, created = OrderProduct.objects.get_or_create(
        product=product,
        user=request.user,
        seller_id=product.user.id,
        ordered=False,
        # quantity=quantity
        # user__account__slug=slug
    )
    # order_qs = Order.objects.filter(user=request.user, ordered=False, seller=product.user)
    order_qs = Order.objects.filter(user=request.user, ordered=False)
    if order_qs.exists():
        order = order_qs[0]

        # check if the order item is in the order
        if order.products.filter(product__id=product.id).exists():
            print("Dilippppppppppppppppppppppppppppppppppp")

            order_product.quantity += int(product.min_order_quantity)
            print(order_product.quantity)
            order_product.save()
            messages.info(request, "This item quantity was updated.")
            return redirect("dashboard:cart", slug=product.user.account.slug)
        else:
            messages.info(request, "This item was added to your cart.")
            order.products.add(order_product)
            return redirect("dashboard:cart", slug=product.user.account.slug)

    else:
        ordered_date = timezone.now()
        # order = Order.objects.create(user=request.user, seller=product.user, ordered_date=ordered_date)
        order = Order.objects.create(user=request.user, ordered_date=ordered_date)
        order.products.add(order_product)
        messages.info(request, "This item was added to your cart.")
        return redirect("dashboard:cart", slug=product.user.account.slug)

@login_required
def remove_from_cart(request, slug, pk):
    product = get_object_or_404(Product, id=pk)
    order_qs = Order.objects.filter(
        user=request.user,
        ordered=False
    )
    if order_qs.exists():
        order = order_qs[0]
        # check if the order item is in the order
        if order.products.filter(product__id=product.id).exists():
            order_product = OrderProduct.objects.filter(
                product=product,
                user=request.user,
                ordered=False,
                # seller = product.user.account.slug
            )[0]
            order.products.remove(order_product)
            order_product.delete()
            del(order_product)
            messages.info(request, "This item was removed from your cart.")
            return redirect("dashboard:cart", slug=product.user.account.slug)
        else:
            messages.info(request, "This item was not in your cart")
            return redirect("dashboard:seller_product_detail", pk=pk)
    else:
        messages.info(request, "You do not have an active order")
        return redirect("dashboard:seller_product_detail", pk=pk)

class CartView(LoginRequiredMixin, View):
    def get(self, *args, **kwargs):
        try:
            order = Order.objects.get(user=self.request.user, ordered=False)
            context = {
                'object': order
            }
            return render(self.request, 'company/cart.html', context)
        except ObjectDoesNotExist:
            messages.warning(self.request, "You do not have an active order")
            return redirect("/")

@login_required
def remove_single_product_from_cart(request,slug, pk):
    product = get_object_or_404(Product, id=pk)
    order_qs = Order.objects.filter(
        user=request.user,
        ordered=False
    )
    if order_qs.exists():
        order = order_qs[0]
        # check if the order item is in the order
        if order.products.filter(product__id=product.id).exists():
            order_product = OrderProduct.objects.filter(
                product=product,
                user=request.user,
                ordered=False
            )[0]
            if order_product.quantity > int(product.min_order_quantity):
                order_product.quantity -= int(product.min_order_quantity)
                order_product.save()
            else:
                order.products.remove(order_product)
            messages.info(request, "This item quantity was updated.")
            return redirect("dashboard:cart", slug=product.user.account.slug)
        else:
            messages.info(request, "This item was not in your cart")
            return redirect("dashboard:seller_product_detail", pk=pk)
    else:
        messages.info(request, "You do not have an active order")
        return redirect("dashboard:seller_product_detail", pk=pk)

class SellerOrderListView(ListView):
    model = Product
    template_name = "dashboard/seller/seller_order_list.html"

    def get_queryset(self, *args, **kwargs):
        qs = super(SellerOrderListView, self).get_queryset(*args, **kwargs)
        if self.request.user.is_authenticated:
            qs = Order.objects.filter(seller=self.request.user)[:1]
        else:
            print("You are Fake")
        return qs

class SellerOrderDetailView(View):
    def get(self, request, *args, **kwargs):
        object_list = Order.objects.filter(seller=self.request.user)
        return render(self.request, "dashboard/seller/seller_order_detail.html", {'object_list':object_list})

class GeneratePdf(View):
    def get(self, request, *args, **kwargs):
        order =Order.objects.get(ordered=True, seller = self.kwargs['pk'])

        context = {
             'order': order,
        }
        pdf = render_to_pdf('dashboard/seller/invoice.html', context)
        return HttpResponse(pdf, content_type='application/pdf')

class SellerOrderConfirmedView(View):
    def get(self,*args, **kwargs):
        order = Order.objects.get(id=self.kwargs['pk'])
        order.status = "Order confirmed"
        order.save()
        return HttpResponseRedirect(self.request.META.get('HTTP_REFERER'))

class SellerOrderProcessingView(View):
    def get(self,*args, **kwargs):
        order = Order.objects.get(id=self.kwargs['pk'])
        order.status = "Processing order"
        order.save()
        return HttpResponseRedirect(self.request.META.get('HTTP_REFERER'))

class SellerQualityCheckView(View):
    def get(self,*args, **kwargs):
        order = Order.objects.get(id=self.kwargs['pk'])
        order.status = "Quality check"
        order.save()
        return HttpResponseRedirect(self.request.META.get('HTTP_REFERER'))

class SellerProductDispatchView(View):
    def get(self,*args, **kwargs):
        order = Order.objects.get(id=self.kwargs['pk'])
        order.status = "Product dispatched"
        order.save()
        return HttpResponseRedirect(self.request.META.get('HTTP_REFERER'))

class SellerProducrDeliveredView(View):
    def get(self,*args, **kwargs):
        order = Order.objects.get(id=self.kwargs['pk'])
        order.status = "Product delivered"
        order.save()
        return HttpResponseRedirect(self.request.META.get('HTTP_REFERER'))

class GenerateOtpView(View):
    def post(self,*args, **kwargs):
        mobile = self.request.POST['mobile']
        generated_otp = random.randint(1111, 9999)
        print('generated otp is ', generated_otp)

        url = "https://www.fast2sms.com/dev/bulk"
        payload = "sender_id=FSTSMS&message=OTP for B2BMART account is {} &language=english&route=p&numbers={}".format(
            generated_otp,
            mobile)
        headers = {
            'authorization': "Y2VwtWGO19bLzcMrTefIj8x5kpJg3hKyvDQ6UBHdRPECN4unisgfqzcs8ENnG7kAlJQy3pYDeIjLTBwU",
            'Content-Type': "application/x-www-form-urlencoded",
            'Cache-Control': "no-cache",
        }
        response = requests.request("POST", url, data=payload, headers=headers)
        return render(self.request, 'verify_otp.html', {'generated_otp':generated_otp})

    def get(self,*args, **kwargs):
        return render(self.request, 'otp.html')

class VerifyOtpView(View):
    def post(self,*args, **kwargs):
        user_otp1 = self.request.POST['user_otp1']
        user_otp2 = self.request.POST['user_otp2']
        user_otp3 = self.request.POST['user_otp3']
        user_otp4 = self.request.POST['user_otp4']
        user_otp = user_otp1 + user_otp2 + user_otp3 + user_otp4
        print(user_otp)

        generated_otp = self.request.POST['generated_otp']
        print(generated_otp)
        if user_otp == generated_otp:
            account = Account.objects.get(user=self.request.user)
            account.otp_verified = True
            account.save()

            messages.success(self.request, "verified successfully")
            if self.request.user.account.business_type == "Seller":
                return redirect('dashboard:seller_contact_profile')
            if self.request.user.account.business_type == "Buyer":
                return redirect('accounts:buyer_profile')
        messages.warning(self.request, "otp not matching")
        return render(self.request, 'verify_otp.html', {'generated_otp':generated_otp})

# from .models import Color
# class ColorView(View):
#     def post(self, *args, **kwargs):
#         if self.request.user.account.slug == self.kwargs['slug']:
#             background = self.request.POST['background']
#             button = self.request.POST['button']
#             print(background)
#             print(button)
#             try:
#                 color = Color.objects.get(user=self.request.user)
#                 color.background = background
#                 color.button = button
#                 color.save()
#                 return HttpResponseRedirect(self.request.META.get('HTTP_REFERER'))
#             except:
#                 color = Color(user=self.request.user, background=background, button=button)
#                 color.save()
#                 return HttpResponseRedirect(self.request.META.get('HTTP_REFERER'))

def credit_terms_view(request):
    return render(request, 'company/credit_terms.html')


######################################################
from .forms import ProductForm, PriceFormSet
from product.models import Price

#add
class ProductCreateView(View):
    def get(self, request, *args, **kwargs):
        form = ProductForm()
        price_form = PriceFormSet()
        return render(request, 'dashboard/seller/aadd_product.html',{'form':form, 'price_form': price_form})        

    def post(self, request, *args, **kwargs):
        form = ProductForm(self.request.POST, self.request.FILES)
        price_form = PriceFormSet(self.request.POST, self.request.FILES)
        if form.is_valid() and price_form.is_valid():
            form = form.save(commit=False)
            form.user=self.request.user

            account = Account.objects.get(user=self.request.user)
            form.slug=account.slug
            form.save()
            product = Product.objects.filter(slug=account.slug).last()

            price_form.instance = form
            price_form.save()

            price = Price.objects.filter(product=product)
            for p in price:
                p.product=product
                p.name = product.name
                p.slug = product.slug
                p.user = request.user
                p.save()
            messages.success(request, 'Upload success')
            return redirect(".")
        else:
            messages.warning(request, 'invalid form')
            return redirect(".")


#update

class ProductUpdateView(View):

    def get(self, request, *args, **kwargs):
        form = ProductForm(instance=Product.objects.get(pk=self.kwargs['pk']))
        price_form = PriceFormSet(instance = Product.objects.get(pk=self.kwargs['pk']))
        return render(request, 'dashboard/seller/update_product.html',{'form':form, 'price_form': price_form})        

    def post(self, request, *args, **kwargs):
        self.object = None
        form = ProductForm(self.request.POST, self.request.FILES,  instance=Product.objects.get(pk=self.kwargs['pk']))
        price_form = PriceFormSet(self.request.POST, self.request.FILES, instance = Product.objects.get(pk=self.kwargs['pk']))
        if form.is_valid() and price_form.is_valid():
            form = form.save()
            product = Product.objects.get(pk=self.kwargs['pk'])
            price_form.instance = form
            price_form.save(commit=False)
            price_form.user = self.request.user
            price_form.save()
            product = Product.objects.get(pk=self.kwargs['pk'])
            
            price = Price.objects.filter(product=product)
            for p in price:
                p.product=product
                p.name = product.name
                p.slug = product.slug
                p.user = request.user
                p.save()
            messages.success(request, 'update success')
            return redirect(".")
        else:
            messages.warning(request, 'invalid form')
            return redirect(".")
###############################################################################

#########################Seller Paid Service###################################
def seller_paid_service():
    return HttpResponse('seller_paid_service')
#########################End Seller Paid Service###############################

