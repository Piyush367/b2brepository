from django.db import models
from accounts.models import Account
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User
from embed_video.fields import EmbedVideoField


class SellerProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    promoter_first_name = models.CharField(max_length=50)
    promoter_last_name = models.CharField(max_length=50)
    designation = models.CharField(max_length=50)
    address_building = models.CharField(max_length=100)
    address_area = models.CharField(max_length=100)
    landmark = models.CharField(max_length=50)
    locality = models.CharField(max_length=50)
    country = models.CharField(max_length=50)
    city = models.CharField(max_length=50)
    gstin = models.CharField(max_length=50, blank=True, null=True, default=None)
    company_website = models.CharField(max_length=50)
    alternative_mobile = models.CharField(max_length=50)
    alternative_email = models.EmailField(max_length=50)
    landline_no = models.CharField(max_length=50)
    alternative_landline_no = models.CharField(max_length=50)
    about_me = models.TextField()
    approve = models.BooleanField(default=False)

    def __str__(self):
        return self.user.username



def validate_image(fieldfile_obj):
    filesize = fieldfile_obj.file.size
    megabyte_limit = 2.0
    if filesize > megabyte_limit*1024*1024:
        raise ValidationError("Max file size is %sMB" % str(megabyte_limit))


class BusinessProfile(models.Model):
    CATEGORY = (
			('Seller', 'Seller'),
			('Manufacturer', 'Manufacturer'),
			) 
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='businessprofile')
    year_of_establishment = models.DateField(blank=True, null=True)
    phone = models.CharField(max_length=200, blank=True, null=True)
    category = models.CharField(max_length=200, blank=True, null=True, choices=CATEGORY)
    annual_turnover = models.CharField(max_length=200, blank=True, null=True)
    company_card_front_view = models.ImageField(upload_to='company_card_images', null=True, blank=True)
    company_card_back_view = models.ImageField(upload_to='company_card_images', null=True, blank=True)
    
    def __str__(self):
        return self.user.username

      
class SellerStatutory(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    gst_no = models.CharField(max_length=50, null=True, blank=True)
    pan_no = models.CharField(max_length=50, null=True)
    tan_no = models.CharField(max_length=50, null=True)
    cin_no = models.CharField(max_length=100, null=True)
    dgft_ie_code = models.CharField(max_length=100, null=True)
    company_registration_no = models.URLField(max_length=50, null=True)

    def __str__(self):
        return self.user.username
    

class SellerBank(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    bank_name = models.CharField(max_length=50)
    account_no = models.CharField(max_length=50)
    bank_account_name = models.CharField(max_length=50)
    ifsc_code = models.CharField(max_length=50)
    account_type = models.CharField(max_length=50)
    alternative_bank_name = models.CharField(max_length=50, blank=True, null=True)
    alternative_account_no = models.CharField(max_length=50, blank=True, null=True)
    alternative_bank_account_name = models.CharField(max_length=50, blank=True, null=True)
    alternative_bank_ifsc_code = models.CharField(max_length=50, blank=True, null=True)
    alternative_bank_account_type = models.CharField(max_length=50, blank=True, null=True)

    def __str__(self):
        return self.user.username



from django.db.models.signals import post_save
def businessprofile_receiver(sender, instance, created, *args, **kwargs):
    if created:
        userprofile = BusinessProfile.objects.create(user=instance)


post_save.connect(businessprofile_receiver, sender=User)

class SellerCompany(models.Model):
    CATEGORY = (
			('Proprietorship', 'Proprietorship'),
			('Partnership', 'Partnership'),
            ('LLP', 'LLP'),
            ('OPC', 'OPC'),
            ('Private Ltd', 'Private Ltd'),
            ('Limited', 'Limited'),
			) 
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='sellercompany')
    about_seller = models.TextField(blank=True, null=True)
    no_of_employees = models.PositiveIntegerField(blank=True, null=True)
    legal_status_of_firm = models.CharField(max_length=200, blank=True, null=True, choices=CATEGORY)
    catalogue = models.FileField(upload_to="files/seller_catalogues", blank=True, null=True)
    branded_video = EmbedVideoField(null=True)
<<<<<<< HEAD
    
=======
>>>>>>> d1b97b0fb238df1f15884ed799e4ff6b9878695f
    caption = models.CharField(max_length=200, blank=True, null=True)
    logo = models.ImageField(default="images/seller_banner_images/imgseller.png", upload_to="images/seller_logos", blank=True, null=True)
    banner_image =models.ImageField(default="images/seller_banner_images/imgseller.png", upload_to="images/seller_banner_images", blank=True, null=True)
     
    def __str__(self):
        return self.user.username


def sellercompany_receiver(sender, instance, created, *args, **kwargs):
    if created:
        userprofile = SellerCompany.objects.create(user=instance)

post_save.connect(sellercompany_receiver, sender=User)

BUSINESS_TYPE_CHOICES = (
        ('Manufacturer', 'Manufacturer'),
        ('Retailer', 'Retailer'),
        ('Distributer', 'Distributer'),
        ('Wholeseller', 'Wholeseller'),
        ('Exporter', 'Exporter'),
)

# llist = Language.objects.filter(status=True)
# list1 = []
# for rs in llist:
#     list1.append((rs.code,rs.name))
# langlist = (list1)


account = Account.objects.all()
list1 = []
# for a in account:
    # list1.append((a.slug, a.slug))
SELLER_LIST = (list1)
print("Dilip", SELLER_LIST)
for i in SELLER_LIST:
    print(i[1])
class ShareDetail(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True, related_name="shareuser")
    seller = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True, related_name="shareseller")
    name = models.CharField(max_length=50, null=True, blank=True)
    email = models.EmailField(max_length=100)
    mobile = models.IntegerField()
    business_type = models.CharField(max_length=20, choices=BUSINESS_TYPE_CHOICES)
    message = models.TextField()
    send_copy = models.BooleanField(default=False)
    approve = models.BooleanField(default=False)
    send_to = models.CharField(max_length=50, choices=SELLER_LIST, blank=True, null=True)
    
    def __str__(self):
        return str(self.name) 
  
class Color(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    button = models.CharField(max_length=20,default="")
    background = models.CharField(max_length=20,default="#f5f5f5;")

    def __str__(self):
        return self.user.username
