from django import forms
from .models import *
from product.models import Product
class SellerBankForm(forms.ModelForm):
    class Meta:
        model = SellerBank
        fields = ['bank_name', 'account_no', 'bank_account_name', 'ifsc_code', 
        'account_type', 'alternative_bank_name', 'alternative_account_no', 
        'alternative_bank_account_name', 'alternative_bank_ifsc_code', 'alternative_bank_account_type']

class SellerStatutoryForm(forms.ModelForm):
    class Meta:
        model = SellerStatutory
        fields = ['gst_no', 'pan_no', 'tan_no', 'cin_no', 'dgft_ie_code', 'company_registration_no']


        


class BusinessProfileForm(forms.ModelForm):
    CATEGORY = (
			('Seller', 'Seller'),
			('Manufacturer', 'Manufacturer'),
			) 
    year_of_establishment = forms.DateField( localize=True, widget=forms.DateInput(format = '%Y-%m-%d', attrs={'class': '', 'type':'date'}), required=True)
    phone = forms.IntegerField(widget=forms.NumberInput( attrs={'class': 'form-control'}), required=False)
    category = forms.ChoiceField(initial='Select Value', widget=forms.Select( attrs={'class': 'form-control'}), choices=CATEGORY,  required=True)
    annual_turnover = forms.CharField(widget=forms.TextInput( attrs={'class': 'form-control'}), required=False)
    company_card_front_view = forms.ImageField(widget=forms.FileInput( attrs={'class': '','id':"real-file",'hidden':"hidden"}), required=False)
    company_card_back_view = forms.ImageField(widget=forms.FileInput( attrs={'class': '','id':"real-file1",'hidden':"hidden"}), required=False)

    class Meta:
        model = BusinessProfile
        fields = "__all__"
        exclude = ['user']


class SellerCompanyForm(forms.ModelForm):
    CATEGORY = (
                    ('Proprietorship', 'Proprietorship'),
                    ('Partnership', 'Partnership'),
                    ('LLP', 'LLP'),
                    ('OPC', 'OPC'),
                    ('Private Ltd', 'Private Ltd'),
                    ('Limited', 'Limited'),
			    )
    about_seller = forms.CharField(widget=forms.TextInput( attrs={'class': 'form-control','name':'about', 'id':'about_seller'}), required=True)
    no_of_employees = forms.IntegerField(widget=forms.NumberInput( attrs={'class': 'form-control','name':'num', 'id':'noofemployees'}), required=True)
    legal_status_of_firm = forms.ChoiceField(initial='Select Value', widget=forms.Select( attrs={'class': 'form-control'}), choices=CATEGORY,  required=True)
    catalogue = forms.FileField(widget=forms.FileInput( attrs={'class': 'form-control','name':'catalogue','id':"real-file",'hidden':"hidden"}), required=False)
    branded_video = forms.URLField(widget=forms.URLInput(attrs={'class': 'form-control', 'name':'video' ,'id':"branded_video"}), required=False)
    upload_video = forms.FileField(widget=forms.FileInput(attrs={'class': 'form-control',  'id':""}), required=False)
    logo = forms.ImageField(widget=forms.FileInput( attrs={'class': 'form-control','name':'logo','id':"real-file1",'hidden':"hidden"}), required=False)
    banner_image = forms.ImageField(widget=forms.FileInput( attrs={'class': 'form-control','name':'banner','id':"real-file2",'hidden':"hidden"}), required=False)

    class Meta:
        model = SellerCompany
        fields = "__all__"
        exclude = ['user']



class SellerSingleProductViewForm(forms.ModelForm):
    PRODUCT_GROUP_CHOICES = (
    ("Kitchen Stoves", "Kitchen Stoves"),
    ("Mixer Grinder", "Mixer Grinder"),
    ("Rice Cookers", "Rice Cookers"),
    ("Electric Kettles", "Electric Kettles"),
    ("OTG", "OTG"),
    ("Juicers", "Juicers"),
    ("Blenders", "Blenders"),
    ("Water Heaters", "Water Heaters"),
    ("Water Filters", "Water Filters"),
    ("Induction Cookers", "Induction Cookers"),
    ("Exhaust Hoods", "Exhaust Hoods"),
    ("Sandwich Makers", "Sandwich Makers"),
    ("Toaster", "Toaster"),
    ("Deep Fryers", "Deep Fryers"),
    ("Coffee Makers", "Coffee Makers"),
    ("Electric Iron", "Electric Iron"),
    ("Vaccum Cleaner", "Vaccum Cleaner"),
    ("Air Purifiers", "Air Purifiers"),

    ("Exhaust Fans", "Exhaust Fans"),
    ("Room Heaters", "Room Heaters"),
    ("Table Fans", "Table Fans"),
    ("Wall Fans", "Wall Fans"),
    ("Electric Steamers & Vaporizers", "Electric Steamers & Vaporizers"),
    ("Air Coolers", "Air Coolers"),
    )

    PRODUCT_BRAND_CHOICES = (
        ('Prestige', 'Prestige'),
        ('Padmini', 'Padmini'),
        ('other', 'other'),
    )
    CAPACITY_CHOICES = (
        ("200ml", "200 ml"),
        ("500ml", "500 ml"),
        ("1ltr", "1 ltr")
    )

    MATERIAL_CHOICES = (
        ("polycarbonate", "Polycarbonate"),
        ("PET", "PET"),
        ("other", "Other")
    )

    name = forms.CharField(widget=forms.TextInput( attrs={'class': 'form-control', 'id':''}), required=True)
    price = forms.IntegerField(widget=forms.NumberInput( attrs={'class': 'form-control', 'id':''}), required=True)
    unit_type = forms.CharField(widget=forms.TextInput( attrs={'class': 'form-control', 'id':''}), required=False)
    min_order_quantity = forms.IntegerField(widget=forms.NumberInput( attrs={'class': 'form-control', 'id':''}), required=False)
    product_group = forms.ChoiceField(initial='Select Value', widget=forms.Select( attrs={'class': 'form-control'}), choices=PRODUCT_GROUP_CHOICES,  required=True)
    description = forms.CharField(widget=forms.TextInput( attrs={'class': 'form-control', 'id':''}), required=False)
    capacity = forms.ChoiceField(initial='Select Value', widget=forms.Select( attrs={'class': 'form-control'}), choices=CAPACITY_CHOICES,  required=False)
    brand = forms.ChoiceField(initial='Select Value', widget=forms.Select( attrs={'class': 'form-control'}), choices=PRODUCT_BRAND_CHOICES,  required=True)
    material = forms.ChoiceField(initial='Select Value', widget=forms.Select( attrs={'class': 'form-control'}), choices=MATERIAL_CHOICES,  required=False)
    color = forms.CharField(widget=forms.TextInput( attrs={'class': 'form-control', 'id':''}), required=False)
    size = forms.CharField(widget=forms.TextInput( attrs={'class': 'form-control', 'id':''}), required=False)
    model_no = forms.CharField(widget=forms.TextInput( attrs={'class': 'form-control', 'id':''}), required=False)
    power = forms.CharField(widget=forms.TextInput( attrs={'class': 'form-control', 'id':''}), required=False)
    warranty = forms.CharField(widget=forms.TextInput( attrs={'class': 'form-control', 'id':''}), required=False)
    neck_size = forms.CharField(widget=forms.TextInput( attrs={'class': 'form-control', 'id':''}), required=False)
    closure_type = forms.CharField(widget=forms.TextInput( attrs={'class': 'form-control', 'id':''}), required=False)
    product_code = forms.CharField(widget=forms.TextInput( attrs={'class': 'form-control', 'id':''}), required=False)
    packing_details = forms.CharField(widget=forms.TextInput( attrs={'class': 'form-control', 'id':''}), required=False)
    # rating = forms.CharField(widget=forms.TextInput( attrs={'class': 'form-control', 'id':''}), required=False)
    preffered_search_keyword = forms.CharField(widget=forms.TextInput( attrs={'class': 'form-control', 'id':''}), required=True)
    # local_video_url = forms.FileField(widget=forms.FileInput(attrs={'class': 'form-control',  'id':""}), required=False)
    video_url = forms.URLField(widget=forms.URLInput(attrs={'class': 'form-control',  'id':""}), required=False)
    image1 = forms.ImageField(widget=forms.FileInput( attrs={'class': 'form-control'}), required=False)
    image2 = forms.ImageField(widget=forms.FileInput( attrs={'class': 'form-control'}), required=False)
    image3 = forms.ImageField(widget=forms.FileInput( attrs={'class': 'form-control'}), required=False)


    class Meta:
        model = Product
        fields = ['name', 'price', 'unit_type', 'min_order_quantity', 'product_group', 'description', 
        'capacity','material','brand','color','size', 'model_no', 'power', 'warranty', 'neck_size', 'closure_type', 'product_code', 'packing_details',
          'video_url', 'image1', 'image2', 'image3', 'preffered_search_keyword']


###################################################
from django.forms.models import inlineformset_factory
from product.models import Price

PRODUCT_GROUP_CHOICES = (
    ("Kitchen Stoves", "Kitchen Stoves"),
    ("Mixer Grinder", "Mixer Grinder"),
    ("Rice Cookers", "Rice Cookers"),
    ("Electric Kettles", "Electric Kettles"),
    ("OTG", "OTG"),
    ("Juicers", "Juicers"),
    ("Blenders", "Blenders"),
    ("Water Heaters", "Water Heaters"),
    ("Water Filters", "Water Filters"),
    ("Induction Cookers", "Induction Cookers"),
    ("Exhaust Hoods", "Exhaust Hoods"),
    ("Sandwich Makers", "Sandwich Makers"),
    ("Toaster", "Toaster"),
    ("Deep Fryers", "Deep Fryers"),
    ("Coffee Makers", "Coffee Makers"),
    ("Electric Iron", "Electric Iron"),
    ("Vaccum Cleaner", "Vaccum Cleaner"),
    ("Air Purifiers", "Air Purifiers"),

    ("Exhaust Fans", "Exhaust Fans"),
    ("Room Heaters", "Room Heaters"),
    ("Table Fans", "Table Fans"),
    ("Wall Fans", "Wall Fans"),
    ("Electric Steamers & Vaporizers", "Electric Steamers & Vaporizers"),
    ("Air Coolers", "Air Coolers"),
    )
# PRODUCT_BRAND_CHOICES = (
#     ('Prestige', 'Prestige'),
#     ('Padmini', 'Padmini'),
#     ('other', 'other'),
# )
CAPACITY_CHOICES = (
    ("200ml", "200 ml"),
    ("500ml", "500 ml"),
    ("1ltr", "1 ltr")
)

MATERIAL_CHOICES = (
    ("polycarbonate", "Polycarbonate"),
    ("PET", "PET"),
    ("other", "Other")
)
PRODUCT_BRAND_CHOICES = (
    ('Bajaj Electricals Limited', 'Bajaj Electricals Limited'),
    ('Butterfly Gandhimathi Appliances Limited', 'Butterfly Gandhimathi Appliances Limited'),
    ('Faber India', 'Faber India'),
    ('Fabiano', 'Fabiano'),
    ('Glen India', 'Glen India'),
    ('Greenchef', 'Greenchef'),
    (' Murugan Industries', ' Murugan Industries'),
    ('Padmini Appliances', 'Padmini Appliances'),
    ('Stovecraft', 'Stovecraft'),
    ('Pigeon', 'Pigeon'),
    ('Sunflame', 'Sunflame'),
    ('Surya Flame', 'Surya Flame'),
    ('TTK Prestige', 'TTK Prestige'),
    ('Milton', 'Milton'),

    ('Maharaha', 'Maharaha'),
    ('Bosch', 'Bosch'),
    ('Preethi', 'Preethi'),
    ('Sujata', 'Sujata'),
    ('Prestige', 'Prestige'),
    ('Butterfly Gandhimathi', 'Butterfly Gandhimathi'),
    ('Kitchen Appliances', 'Kitchen Appliances'),
    ('Havells', 'Havells'),
    ('Jaipan', 'Jaipan'),
    ('Usha', 'Usha'),
    ('Crompton Greaves', 'Crompton Greaves'),

    ('Prestige', 'Prestige'),
    ('Inalsa', 'Inalsa'),
    ('Sheffield', 'Sheffield'),
    ('Bajaj', 'Bajaj'),
    ('Pigeon', 'Pigeon'),
    ('Wonderchef', 'Wonderchef'),
    ('Borosil', 'Borosil'),

    ('Preethi', 'Preethi'),
    ('Bajaj', 'Bajaj'),
    ('Maharaja', 'Maharaja'),
    ('Butterfly', 'Butterfly'),
    ('Kenstar', 'Kenstar'),
    ('Havells', 'Havells'),
    ('Usha', 'Usha'),
    ('Crompton', 'Crompton'),
    ('Inalsa', 'Inalsa'),
    ('Wonderchef', 'Wonderchef'),
    ('Prestige', 'Prestige'),

    ('Bajaj', 'Bajaj'),
    ('Wonderchef', 'Wonderchef'),
    ('Singer Uno', 'Singer Uno'),
    ('Maharaja Whiteline', 'Maharaja Whiteline'),
    ('Warmex', 'Warmex'),
    ('Usha', 'Usha'),
    ('Bajaj', 'Bajaj'),
    ('Oreva Kitchof', 'Oreva Kitchof'),
    ('Cromptn', 'Cromptn'),
    ('RNG', 'RNG'),

    ('Preethi', 'Preethi'),
    ('Bajaj', 'Bajaj'),
    ('Bajaj', 'Bajaj'),
    ('Butterfly Gandhimathi', 'Butterfly Gandhimathi'),
    ('Kitchen Appliances', 'Kitchen Appliances'),
    ('Havells', 'Havells'),
    ('Jaipan', 'Jaipan'),
    ('Usha', 'Usha'),
    ('Crompton Greaves', 'Crompton Greaves'),
    ('TTK Prestige ', 'TTK Prestige '),

    ('Preethi', 'Preethi'),
    ('Bajaj', 'Bajaj'),
    ('Philips', 'Philips'),
    ('Maharaja', 'Maharaja'),
    ('Butterfly and Kenstar', 'Butterfly and Kenstar'),

    ('Midea', 'Midea'),
    ('Joyoung', 'Joyoung'),
    ('Panasonic', 'Panasonic'),
    ('SUPOR', 'SUPOR'),
    ('BRAUN', 'BRAUN'),
    ('ACA', 'ACA'),
    ('Deer', 'Deer'),
    ('KENWOOD', 'KENWOOD'),
    ('Bear', 'Bear'),
    ('Royalstar', 'Royalstar'),
    ('Electrolux', 'Electrolux'),
    ('SKG', 'SKG'),
    ('OUKE', 'OUKE'),
    ('AUX', 'AUX'),
    ('KONKA', 'KONKA'),
    ('Whirlpool', 'Whirlpool'),
    ('EUPA', 'EUPA'),
    ('BOSCH', 'BOSCH'),
    ('WELHOME', 'WELHOME'),

    ('Bajaj', 'Bajaj'),
    ('V-Guard', 'V-Guard'),
    ('Havells', 'Havells'),
    ('Crompton', 'Crompton'),
    ('Venus', 'Venus'),
    ('Usha', 'Usha'),
    ('Maharaja', 'Maharaja'),
    ('Hindware', 'Hindware'),
    ('Orient', 'Orient'),
    ('Jaguar', 'Jaguar'),
    ('Kenstar', 'Kenstar'),
    ('Polar', 'Polar'),
    ('Padmini', 'Padmini'),
    ('khaitan', 'khaitan'),

    ('Eureka Forbes', 'Eureka Forbes'),
    ('Ion Exchange', 'Ion Exchange'),
    ('Adhunik', 'Adhunik'),
    ('Aqua fresh', 'Aqua fresh'),
    ('Filmtech', 'Filmtech'),
    ('Kelvinator Quanta', 'Kelvinator Quanta'),
    ('Mannya Grand Plus', 'Mannya Grand Plus'),
    ('Okaya', 'Okaya'),
    ('Sajal', 'Sajal'),
    ('Ventair', 'Ventair'),
    ('Hindustan Unilever', 'Hindustan Unilever'),
    ('Kent RO', 'Kent RO'),
    ('Whirlpool', 'Whirlpool'),
    ('TATA Chemical', 'TATA Chemical'),
    ('3M', '3M'),
    ('Apex', 'Apex'),
    ('Expert', 'Expert'),
    ('Godrej', 'Godrej'),
    ('Krona Liquetac', 'Krona Liquetac'),
    ('Livpure pep plus', 'Livpure pep plus'),
    ('Nasaka Xtra', 'Nasaka Xtra'),
    ('Own', 'Own'),
    ('Propello Uno', 'Propello Uno'),
    ('Secure Water Purifier ', 'Secure Water Purifier '),

    ('Stovekraft', 'Stovekraft'),
    ('Groupe SEB', 'Groupe SEB'),
    ('Havells', 'Havells'),
    ('Butterfly Gandhimathi', 'Butterfly Gandhimathi'),
    ('Kitchen Appliances India Limited', 'Kitchen Appliances India Limited'),
    ('Prestige', 'Prestige'),
    ('Bajaj', 'Bajaj'),
    ('Preethi', 'Preethi'),
    ('Pigeon', 'Pigeon'),

    ('Kaff', 'Kaff'),
    ('Glen', 'Glen'),
    ('Hindware', 'Hindware'),
    ('IFB', 'IFB'),
    ('Bajaj', 'Bajaj'),
    ('Usha', 'Usha'),
    ('Prestige', 'Prestige'),

    ('Borosil', 'Borosil'),
    ('Prestige', 'Prestige'),
    ('Nova', 'Nova'),
    ('Pigeon', 'Pigeon'),
    ('Bajaj', 'Bajaj'),
    ('Lifelong', 'Lifelong'),
    ('Eveready', 'Eveready'),
    ('Wonderchef', 'Wonderchef'),
    ('Inalsa', 'Inalsa'),

    ('Inalsa', 'Inalsa'),
    ('Prestige', 'Prestige'),
    ('Sunflame', 'Sunflame'),
    ('Bajaj', 'Bajaj'),
    ('Oster', 'Oster'),
    ('Havells', 'Havells'),
    ('Kent', 'Kent'),
    ('Pigeon', 'Pigeon'),
    ('Cello Quick', 'Cello Quick'),
    ('V-Guard', 'V-Guard'),

    ('Prestige', 'Prestige'),
    ('Glen air', 'Glen air'),
    ('Bhavya Enterprises', 'Bhavya Enterprises'),
    ('Skyline', 'Skyline'),
    ('Inalsa', 'Inalsa'),
    ('Orbit', 'Orbit'),
    ('Baltra', 'Baltra'),
    ('Presto', 'Presto'),
    ('American Micronic', 'American Micronic'),
    ('Hi-Tech Appliances', 'Hi-Tech Appliances'),
    ('Kiran Enterprises', 'Kiran Enterprises'),
    ('The Urban Kitchen', 'The Urban Kitchen'),

    ('Inalsa', 'Inalsa'),
    ('Bajaj', 'Bajaj'),
    ('Orpat', 'Orpat'),
    ('Maharaja Whiteline', 'Maharaja Whiteline'),
    ('Kitchen Aid', 'Kitchen Aid'),
    ('Tefal', 'Tefal'),
    ('Kuber Industries', 'Kuber Industries'),

    ('Preethi', 'Preethi'),
    ('Prestige', 'Prestige'),
    ('Wonderchef', 'Wonderchef'),
    ('Havells', 'Havells'),
    ('Technora', 'Technora'),
    ('3-D Creaions', '3-D Creaions'),

    ('Inalsa', 'Inalsa'),
    ('Havells', 'Havells'),
    ('Eveready', 'Eveready'),
    ('Usha', 'Usha'),
    ('Orient', 'Orient'),
    ('Pigeon', 'Pigeon'),
    ('Wonderchef', 'Wonderchef'),
    ('Syska', 'Syska'),
    ('Orpat', 'Orpat'),
    ('Maharaja Whiteline', 'Maharaja Whiteline'),

    ('American Micronic', 'American Micronic'),
    ('Inalsa', 'Inalsa'),
    ('Eureka Forbes', 'Eureka Forbes'),
    ('KENT', 'KENT'),

    ('Dyson', 'Dyson'),
    ('Honeywell', 'Honeywell'),
    ('Mi', 'Mi'),
    ('Cowway', 'Cowway'),
    ('Reliance', 'Reliance'),
    ('Livpure', 'Livpure'),
    ('Kent', 'Kent'),
    ('Airpuro', 'Airpuro'),
    ('Dr. Aeroguard', 'Dr. Aeroguard'),

    ('Havells', 'Havells'),
    ('Phillips', 'Phillips'),
    ('Syska', 'Syska'),
    ('Kemei', 'Kemei'),
    ('Vega', 'Vega'),
    ('Figment', 'Figment'),
    ('Iconic', 'Iconic'),
    ('Jivani Brothers', 'Jivani Brothers'),
    ('Nova', 'Nova'),
    ('Wazdrof', 'Wazdrof'),
    ('Unik Brand', 'Unik Brand'),
    ('Array', 'Array'),
    ('Horlite Quick', 'Horlite Quick'),

    ('Wahl', 'Wahl'),
    ('Spectrum Brands', 'Spectrum Brands'),
    ('Conair Corporation', 'Conair Corporation'),
    ('Andis Company', 'Andis Company'),
    ('VEGA', 'VEGA'),
    ('Sunbeam', 'Sunbeam'),
    ('Havells', 'Havells'),
    ('Nova', 'Nova'),
    ('Syska', 'Syska'),
    ('Lifelong', 'Lifelong'),
    ('Ustraa', 'Ustraa'),
    ('Kemei', 'Kemei'),

    ('Havells', 'Havells'),
    ('Phillips', 'Phillips'),
    ('Syska', 'Syska'),
    ('Kemei', 'Kemei'),
    ('Vega', 'Vega'),
    ('Figment', 'Figment'),
    ('Iconic', 'Iconic'),
    ('Jivani Brothers', 'Jivani Brothers'),
    ('Nova', 'Nova'),
    ('Wazdrof', 'Wazdrof'),
    ('Unik Brand', 'Unik Brand'),
    ('Array', 'Array'),
    ('Horlite Quick', 'Horlite Quick'),
)
class ProductForm(forms.ModelForm):

    # user = forms.CharField(widget=forms.HiddenInput(),initial=1,required=False)
    # name = forms.CharField(widget=forms.TextInput( attrs={'class': 'form-control', 'id':''}), required=True)
    # unit_type = forms.CharField(widget=forms.TextInput( attrs={'class': 'form-control', 'id':''}), required=False)
    # min_order_quantity = forms.IntegerField(widget=forms.NumberInput( attrs={'class': 'form-control', 'id':''}), required=False)
    # product_group = forms.ChoiceField(initial='Select Value', widget=forms.Select( attrs={'class': 'form-control'}), choices=PRODUCT_GROUP_CHOICES,  required=True)
    # description = forms.CharField(widget=forms.TextInput( attrs={'class': 'form-control', 'id':''}), required=False)
    # capacity = forms.ChoiceField(initial='Select Value', widget=forms.Select( attrs={'class': 'form-control'}), choices=CAPACITY_CHOICES,  required=False)
    # brand = forms.ChoiceField(initial='Select Value', widget=forms.Select( attrs={'class': 'form-control'}), choices=PRODUCT_BRAND_CHOICES,  required=True)
    # material = forms.ChoiceField(initial='Select Value', widget=forms.Select( attrs={'class': 'form-control'}), choices=MATERIAL_CHOICES,  required=False)
    # size = forms.CharField(widget=forms.TextInput( attrs={'class': 'form-control', 'id':''}), required=False)
    # model_no = forms.CharField(widget=forms.TextInput( attrs={'class': 'form-control', 'id':''}), required=False)
    # power = forms.CharField(widget=forms.TextInput( attrs={'class': 'form-control', 'id':''}), required=False)
    # warranty = forms.CharField(widget=forms.TextInput( attrs={'class': 'form-control', 'id':''}), required=False)
    # neck_size = forms.CharField(widget=forms.TextInput( attrs={'class': 'form-control', 'id':''}), required=False)
    # closure_type = forms.CharField(widget=forms.TextInput( attrs={'class': 'form-control', 'id':''}), required=False)
    # product_code = forms.CharField(widget=forms.TextInput( attrs={'class': 'form-control', 'id':''}), required=False)
    # packing_details = forms.CharField(widget=forms.TextInput( attrs={'class': 'form-control', 'id':''}), required=False)
    # preffered_search_keyword = forms.CharField(widget=forms.TextInput( attrs={'class': 'form-control', 'id':''}), required=True)
    # video_url = forms.URLField(widget=forms.URLInput(attrs={'class': 'form-control',  'id':""}), required=False)

    # minimum_quantity = forms.IntegerField(widget=forms.NumberInput( attrs={'class': 'form-control', 'id':''}), required=False)
    # maximum_quantity = forms.IntegerField(widget=forms.NumberInput( attrs={'class': 'form-control', 'id':''}), required=False)
    # price = forms.IntegerField(widget=forms.NumberInput( attrs={'class': 'form-control', 'id':''}), required=False)
    # image1 = forms.ImageField(widget=forms.FileInput( attrs={'class': 'form-control'}), required=False)
    # image2 = forms.ImageField(widget=forms.FileInput( attrs={'class': 'form-control'}), required=False)
    # image3 = forms.ImageField(widget=forms.FileInput( attrs={'class': 'form-control'}), required=False)
    # color = forms.CharField(widget=forms.TextInput( attrs={'class': 'form-control', 'id':''}), required=False)

    class Meta:
        model = Product
        fields = ['name', 'product_group', 'brand', 'product_model_no', 'product_type', 'in_the_box',
         'body_material', 'capacity', 'wattage', 'warranty', 'description','plate_material', 'blade_material',
          'product_weight', 'coverage_area', 'product_length', 'product_width', 'cooktop_material', 'no_of_speed', 
          'tank_capacity', 'fan_material', 'ventilation_type', 'dimension', 'suction_capacity', 'no_of_jars',
           'no_of_slices']
        widgets = {
            'name': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
            'product_group': forms.Select( attrs={'class': 'form-control'}, choices=PRODUCT_GROUP_CHOICES),
            'description': forms.Textarea( attrs={'class': 'form-control', 'rows':3}),
            'capacity': forms.Select( attrs={'class': 'form-control'}, choices=CAPACITY_CHOICES),
            'brand': forms.Select( attrs={'class': 'form-control'}, choices=PRODUCT_BRAND_CHOICES),
            'product_model_no': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
            'warranty': forms.TextInput( attrs={'class': 'form-control', 'id':''}),

            'product_type': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
            'in_the_box': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
            'body_material': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
            'plate_material': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
            'blade_material': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
            'product_weight': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
            'coverage_area': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
            'product_length': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
            'product_width': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
            'cooktop_material': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
            'no_of_speed': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
            'tank_capacity': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
            'fan_material': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
            'ventilation_type': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
            'dimension': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
            'suction_capacity': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
            'no_of_jars': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
            'no_of_slices': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
            
           }

class PriceForm(forms.ModelForm):
    minimum_quantity = forms.IntegerField(widget=forms.NumberInput( attrs={'class': 'form-control', 'id':''}), required=False)
    maximum_quantity = forms.IntegerField(widget=forms.NumberInput( attrs={'class': 'form-control', 'id':''}), required=False)
    price_per_piece = forms.IntegerField(widget=forms.NumberInput( attrs={'class': 'form-control', 'id':''}), required=False)
    image1 = forms.ImageField(widget=forms.FileInput( attrs={'class': 'form-control'}), required=False)
    image2 = forms.ImageField(widget=forms.FileInput( attrs={'class': 'form-control'}), required=False)
    image3 = forms.ImageField(widget=forms.FileInput( attrs={'class': 'form-control'}), required=False)
    image4 = forms.ImageField(widget=forms.FileInput( attrs={'class': 'form-control'}), required=False)
    image5 = forms.ImageField(widget=forms.FileInput( attrs={'class': 'form-control'}), required=False)
    color = forms.CharField(widget=forms.TextInput( attrs={'class': 'form-control', 'id':''}), required=False)
    user = forms.CharField(disabled=True, required=False)

    class Meta:
        model = Price
        fields=('minimum_quantity', 'maximum_quantity', 'price_per_piece', 'color', 'image1','image2','image3','image4','image5')

    def save(self, commit=True):
        instance = super(PriceForm, self).save(commit=False)
        instance.user =  self.request.user#flag1' in self.cleaned_data['multi_choice'] # etc
        if commit:
            instance.save()
        return instance
PriceFormSet = inlineformset_factory(Product, Price, form= ProductForm, fields=('minimum_quantity', 'maximum_quantity', 'price_per_piece', 'color', 'image1', 'image2', 'image3', 'image4', 'image5'), extra=1)

    # def save(self, commit=True):
    #     instance = super(ProductForm, self).save(commit=False)
    #     instance.user =  self.request.user#flag1' in self.cleaned_data['multi_choice'] # etc
    #     if commit:
    #         instance.save()
    #     return instance


# class DeepFriersForm(forms.ModelForm):
#     class Meta:
#         model = Product
#         fields = ['name','product_group','brand', 'product_model_no', 'in_the_box', 'body_material', 'capacity', 'wattage', 'warranty', 'description']
#         widgets = {
#             'name': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'product_group': forms.Select( attrs={'class': 'form-control'}, choices=PRODUCT_GROUP_CHOICES),
#             'brand': forms.Select( attrs={'class': 'form-control'}, choices=PRODUCT_BRAND_CHOICES),
#             'product_model_no': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'in_the_box': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'body_material': forms.Select( attrs={'class': 'form-control'}, choices=MATERIAL_CHOICES),
#             'capacity': forms.Select( attrs={'class': 'form-control'}, choices=CAPACITY_CHOICES),
#             'wattage': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'warranty': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'description': forms.Textarea( attrs={'class': 'form-control', 'rows':3}),
#            }
# PriceFormSet = inlineformset_factory(Product, Price, form= DeepFriersForm, fields=('minimum_quantity', 'maximum_quantity', 'price_per_piece', 'color', 'image1', 'image2', 'image3', 'image4', 'image5'), extra=1)

# class ElectricIronForm(forms.ModelForm):
#     class Meta:

#         model = Product
#         fields = ['name','product_group','brand', 'product_model_no', 'in_the_box', 'body_material', 'plate_material', 'product_weight', 'warranty', 'description']
#         widgets = {
#             'name': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'product_group': forms.Select( attrs={'class': 'form-control'}, choices=PRODUCT_GROUP_CHOICES),
#             'brand': forms.Select( attrs={'class': 'form-control'}, choices=PRODUCT_BRAND_CHOICES),
#             'product_model_no': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'in_the_box': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'body_material': forms.Select( attrs={'class': 'form-control'}, choices=MATERIAL_CHOICES),
#             'plate_material': forms.Select( attrs={'class': 'form-control'}, choices=CAPACITY_CHOICES),
#             'product_weight': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'warranty': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'description': forms.Textarea( attrs={'class': 'form-control', 'rows':3}),
#            }
# PriceFormSet = inlineformset_factory(Product, Price, form= ElectricIronForm, fields=('minimum_quantity', 'maximum_quantity', 'price_per_piece', 'color', 'image1', 'image2', 'image3', 'image4', 'image5'), extra=1)


# class AirPurifiersForm(forms.ModelForm):
#     class Meta:

#         model = Product
#         fields = ['name','product_group','brand', 'product_model_no', 'in_the_box', 'body_material', 'coverage_area', 'wattage', 'warranty', 'description']
#         widgets = {
#             'name': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'product_group': forms.Select( attrs={'class': 'form-control'}, choices=PRODUCT_GROUP_CHOICES),
#             'brand': forms.Select( attrs={'class': 'form-control'}, choices=PRODUCT_BRAND_CHOICES),
#             'product_model_no': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'in_the_box': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'body_material': forms.Select( attrs={'class': 'form-control'}, choices=MATERIAL_CHOICES),
#             'coverage_area': forms.Select( attrs={'class': 'form-control'}, choices=CAPACITY_CHOICES),
#             'wattage': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'warranty': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'description': forms.Textarea( attrs={'class': 'form-control', 'rows':3}),
#            }

# PriceFormSet = inlineformset_factory(Product, Price, form= AirPurifiersForm, fields=('minimum_quantity', 'maximum_quantity', 'price_per_piece', 'color', 'image1', 'image2', 'image3', 'image4', 'image5'), extra=1)


# class ExhaustFansForm(forms.ModelForm):
#     class Meta:

#         model = Product
#         fields = ['name','product_group','brand', 'product_model_no', 'product_length', 'product_width', 'blade_material', 'body_material', 'wattage', 'warranty', 'description']
#         widgets = {
#             'name': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'product_group': forms.Select( attrs={'class': 'form-control'}, choices=PRODUCT_GROUP_CHOICES),
#             'brand': forms.Select( attrs={'class': 'form-control'}, choices=PRODUCT_BRAND_CHOICES),
#             'product_model_no': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'product_length': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'product_width': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'blade_material': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'body_material': forms.Select( attrs={'class': 'form-control'}, choices=MATERIAL_CHOICES),
#             'coverage_area': forms.Select( attrs={'class': 'form-control'}, choices=CAPACITY_CHOICES),
#             'wattage': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'warranty': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'description': forms.Textarea( attrs={'class': 'form-control', 'rows':3}),
#            }

# PriceFormSet = inlineformset_factory(Product, Price, form= ExhaustFansForm, fields=('minimum_quantity', 'maximum_quantity', 'price_per_piece', 'color', 'image1', 'image2', 'image3', 'image4', 'image5'), extra=1)


# class JuicersForm(forms.ModelForm):
#     class Meta:

#         model = Product
#         fields = ['name','product_group','brand', 'product_model_no', 'product_length', 'product_width', 'blade_material', 'in_the_box', 'body_material', 'wattage', 'warranty', 'description']
#         widgets = {
#             'name': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'product_group': forms.Select( attrs={'class': 'form-control'}, choices=PRODUCT_GROUP_CHOICES),
#             'brand': forms.Select( attrs={'class': 'form-control'}, choices=PRODUCT_BRAND_CHOICES),
#             'product_model_no': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'blade_material': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'in_the_box': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'body_material': forms.Select( attrs={'class': 'form-control'}, choices=MATERIAL_CHOICES),
#             'blade_material': forms.Select( attrs={'class': 'form-control'}, choices=MATERIAL_CHOICES),
#             'product_weight': forms.Select( attrs={'class': 'form-control'}, choices=MATERIAL_CHOICES),
#             'wattage': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'warranty': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'description': forms.Textarea( attrs={'class': 'form-control', 'rows':3}),
#            }

# PriceFormSet = inlineformset_factory(Product, Price, form= JuicersForm, fields=('minimum_quantity', 'maximum_quantity', 'price_per_piece', 'color', 'image1', 'image2', 'image3', 'image4', 'image5'), extra=1)


# class InductionCookersForm(forms.ModelForm):
#     class Meta:

#         model = Product
#         fields = ['name','product_group','brand', 'product_model_no', 'blade_material', 'blade_material', 'in_the_box', 'body_material', 'cooktop_material', 'wattage', 'warranty', 'description']
#         widgets = {
#             'name': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'product_group': forms.Select( attrs={'class': 'form-control'}, choices=PRODUCT_GROUP_CHOICES),
#             'brand': forms.Select( attrs={'class': 'form-control'}, choices=PRODUCT_BRAND_CHOICES),
#             'product_model_no': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'blade_material': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'in_the_box': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'body_material': forms.Select( attrs={'class': 'form-control'}, choices=MATERIAL_CHOICES),
#             'cooktop_material': forms.Select( attrs={'class': 'form-control'}, choices=MATERIAL_CHOICES),
#             'wattage': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'warranty': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'description': forms.Textarea( attrs={'class': 'form-control', 'rows':3}),
#            }

# PriceFormSet = inlineformset_factory(Product, Price, form= InductionCookersForm, fields=('minimum_quantity', 'maximum_quantity', 'price_per_piece', 'color', 'image1', 'image2', 'image3', 'image4', 'image5'), extra=1)


# class OtgOvenTandoorGrillForm(forms.ModelForm):
#     class Meta:

#         model = Product
#         fields = ['name','product_group','brand', 'product_model_no', 'in_the_box', 'body_material', 'capacity', 'wattage', 'warranty', 'description']
#         widgets = {
#             'name': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'product_group': forms.Select( attrs={'class': 'form-control'}, choices=PRODUCT_GROUP_CHOICES),
#             'brand': forms.Select( attrs={'class': 'form-control'}, choices=PRODUCT_BRAND_CHOICES),
#             'product_model_no': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'in_the_box': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'body_material': forms.Select( attrs={'class': 'form-control'}, choices=MATERIAL_CHOICES),
#             'capacity': forms.Select( attrs={'class': 'form-control'}, choices=MATERIAL_CHOICES),
#             'wattage': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'warranty': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'description': forms.Textarea( attrs={'class': 'form-control', 'rows':3}),
#            }

# PriceFormSet = inlineformset_factory(Product, Price, form= OtgOvenTandoorGrillForm, fields=('minimum_quantity', 'maximum_quantity', 'price_per_piece', 'color', 'image1', 'image2', 'image3', 'image4', 'image5'), extra=1)


# class RoomHeaterForm(forms.ModelForm):
#     class Meta:

#         model = Product
#         fields = ['name','product_group','brand', 'product_model_no', 'in_the_box', 'body_material', 'no_of_speed', 'wattage', 'warranty', 'description']
#         widgets = {
#             'name': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'product_group': forms.Select( attrs={'class': 'form-control'}, choices=PRODUCT_GROUP_CHOICES),
#             'brand': forms.Select( attrs={'class': 'form-control'}, choices=PRODUCT_BRAND_CHOICES),
#             'product_model_no': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'in_the_box': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'body_material': forms.Select( attrs={'class': 'form-control'}, choices=MATERIAL_CHOICES),
#             'no_of_speed': forms.Select( attrs={'class': 'form-control'}, choices=MATERIAL_CHOICES),
#             'wattage': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'warranty': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'description': forms.Textarea( attrs={'class': 'form-control', 'rows':3}),
#            }

# PriceFormSet = inlineformset_factory(Product, Price, form= RoomHeaterForm, fields=('minimum_quantity', 'maximum_quantity', 'price_per_piece', 'color', 'image1', 'image2', 'image3', 'image4', 'image5'), extra=1)


# class TableFansForm(forms.ModelForm):
#     class Meta:

#         model = Product
#         fields = ['name','product_group','brand', 'product_model_no', 'in_the_box', 'body_material', 'blade_material', 'no_of_speed', 'wattage', 'warranty', 'description']
#         widgets = {
#             'name': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'product_group': forms.Select( attrs={'class': 'form-control'}, choices=PRODUCT_GROUP_CHOICES),
#             'brand': forms.Select( attrs={'class': 'form-control'}, choices=PRODUCT_BRAND_CHOICES),
#             'product_model_no': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'in_the_box': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'body_material': forms.Select( attrs={'class': 'form-control'}, choices=MATERIAL_CHOICES),
#             'blade_material': forms.Select( attrs={'class': 'form-control'}, choices=MATERIAL_CHOICES),
#             'no_of_speed': forms.Select( attrs={'class': 'form-control'}, choices=MATERIAL_CHOICES),
#             'wattage': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'warranty': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'description': forms.Textarea( attrs={'class': 'form-control', 'rows':3}),
#            }

# PriceFormSet = inlineformset_factory(Product, Price, form= TableFansForm, fields=('minimum_quantity', 'maximum_quantity', 'price_per_piece', 'color', 'image1', 'image2', 'image3', 'image4', 'image5'), extra=1)


# class WallFansForm(forms.ModelForm):
#     class Meta:

#         model = Product
#         fields = ['name','product_group','brand', 'product_model_no', 'in_the_box', 'body_material', 'blade_material', 'no_of_speed', 'wattage', 'warranty', 'description']
#         widgets = {
#             'name': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'product_group': forms.Select( attrs={'class': 'form-control'}, choices=PRODUCT_GROUP_CHOICES),
#             'brand': forms.Select( attrs={'class': 'form-control'}, choices=PRODUCT_BRAND_CHOICES),
#             'product_model_no': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'in_the_box': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'body_material': forms.Select( attrs={'class': 'form-control'}, choices=MATERIAL_CHOICES),
#             'blade_material': forms.Select( attrs={'class': 'form-control'}, choices=MATERIAL_CHOICES),
#             'no_of_speed': forms.Select( attrs={'class': 'form-control'}, choices=MATERIAL_CHOICES),
#             'wattage': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'warranty': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'description': forms.Textarea( attrs={'class': 'form-control', 'rows':3}),
#            }

# PriceFormSet = inlineformset_factory(Product, Price, form= WallFansForm, fields=('minimum_quantity', 'maximum_quantity', 'price_per_piece', 'color', 'image1', 'image2', 'image3', 'image4', 'image5'), extra=1)


# class WaterHeatersForm(forms.ModelForm):
#     class Meta:

#         model = Product
#         fields = ['name','product_group','brand', 'product_model_no', 'in_the_box', 'body_material', 'tank_capacity', 'wattage', 'warranty', 'description']
#         widgets = {
#             'name': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'product_group': forms.Select( attrs={'class': 'form-control'}, choices=PRODUCT_GROUP_CHOICES),
#             'brand': forms.Select( attrs={'class': 'form-control'}, choices=PRODUCT_BRAND_CHOICES),
#             'product_model_no': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'in_the_box': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'body_material': forms.Select( attrs={'class': 'form-control'}, choices=MATERIAL_CHOICES),
#             'tank_capacity': forms.Select( attrs={'class': 'form-control'}, choices=MATERIAL_CHOICES),
#             'wattage': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'warranty': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'description': forms.Textarea( attrs={'class': 'form-control', 'rows':3}),
#            }

# PriceFormSet = inlineformset_factory(Product, Price, form= WaterHeatersForm, fields=('minimum_quantity', 'maximum_quantity', 'price_per_piece', 'color', 'image1', 'image2', 'image3', 'image4', 'image5'), extra=1)


# class ElectricSteamersVaporizersForm(forms.ModelForm):
#     class Meta:

#         model = Product
#         fields = ['name','product_group','brand', 'product_model_no', 'in_the_box', 'body_material', 'product_weight', 'wattage', 'warranty', 'description']
#         widgets = {
#             'name': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'product_group': forms.Select( attrs={'class': 'form-control'}, choices=PRODUCT_GROUP_CHOICES),
#             'brand': forms.Select( attrs={'class': 'form-control'}, choices=PRODUCT_BRAND_CHOICES),
#             'product_model_no': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'in_the_box': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'body_material': forms.Select( attrs={'class': 'form-control'}, choices=MATERIAL_CHOICES),
#             'product_weight': forms.Select( attrs={'class': 'form-control'}, choices=MATERIAL_CHOICES),
#             'wattage': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'warranty': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'description': forms.Textarea( attrs={'class': 'form-control', 'rows':3}),
#            }

# PriceFormSet = inlineformset_factory(Product, Price, form= ElectricSteamersVaporizersForm, fields=('minimum_quantity', 'maximum_quantity', 'price_per_piece', 'color', 'image1', 'image2', 'image3', 'image4', 'image5'), extra=1)


# class CoffeeMakersForm(forms.ModelForm):
#     class Meta:

#         model = Product
#         fields = ['name','product_group','brand', 'product_model_no', 'in_the_box', 'body_material', 'capacity', 'wattage', 'warranty', 'description']
#         widgets = {
#             'name': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'product_group': forms.Select( attrs={'class': 'form-control'}, choices=PRODUCT_GROUP_CHOICES),
#             'brand': forms.Select( attrs={'class': 'form-control'}, choices=PRODUCT_BRAND_CHOICES),
#             'product_model_no': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'in_the_box': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'body_material': forms.Select( attrs={'class': 'form-control'}, choices=MATERIAL_CHOICES),
#             'capacity': forms.Select( attrs={'class': 'form-control'}, choices=MATERIAL_CHOICES),
#             'wattage': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'warranty': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'description': forms.Textarea( attrs={'class': 'form-control', 'rows':3}),
#            }

# PriceFormSet = inlineformset_factory(Product, Price, form= CoffeeMakersForm, fields=('minimum_quantity', 'maximum_quantity', 'price_per_piece', 'color', 'image1', 'image2', 'image3', 'image4', 'image5'), extra=1)


# class AirCoolersForm(forms.ModelForm):
#     class Meta:

#         model = Product
#         fields = ['name','product_group','brand', 'product_model_no', 'in_the_box', 'fan_material', 'body_material', 'tank_capacity', 'wattage', 'warranty', 'description']
#         widgets = {
#             'name': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'product_group': forms.Select( attrs={'class': 'form-control'}, choices=PRODUCT_GROUP_CHOICES),
#             'brand': forms.Select( attrs={'class': 'form-control'}, choices=PRODUCT_BRAND_CHOICES),
#             'product_model_no': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'in_the_box': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'fan_material': forms.Select( attrs={'class': 'form-control'}, choices=MATERIAL_CHOICES),
#             'body_material': forms.Select( attrs={'class': 'form-control'}, choices=MATERIAL_CHOICES),
#             'tank_capacity': forms.Select( attrs={'class': 'form-control'}, choices=MATERIAL_CHOICES),
#             'wattage': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'warranty': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'description': forms.Textarea( attrs={'class': 'form-control', 'rows':3}),
#            }

# PriceFormSet = inlineformset_factory(Product, Price, form= AirCoolersForm, fields=('minimum_quantity', 'maximum_quantity', 'price_per_piece', 'color', 'image1', 'image2', 'image3', 'image4', 'image5'), extra=1)


# class ElectricKettlesForm(forms.ModelForm):
#     class Meta:

#         model = Product
#         fields = ['name','product_group','brand', 'product_model_no', 'in_the_box', 'body_material', 'capacity', 'wattage', 'warranty', 'description']
#         widgets = {
#             'name': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'product_group': forms.Select( attrs={'class': 'form-control'}, choices=PRODUCT_GROUP_CHOICES),
#             'brand': forms.Select( attrs={'class': 'form-control'}, choices=PRODUCT_BRAND_CHOICES),
#             'product_model_no': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'in_the_box': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'body_material': forms.Select( attrs={'class': 'form-control'}, choices=MATERIAL_CHOICES),
#             'capacity': forms.Select( attrs={'class': 'form-control'}, choices=MATERIAL_CHOICES),
#             'wattage': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'warranty': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'description': forms.Textarea( attrs={'class': 'form-control', 'rows':3}),
#            }

# PriceFormSet = inlineformset_factory(Product, Price, form= ElectricKettlesForm, fields=('minimum_quantity', 'maximum_quantity', 'price_per_piece', 'color', 'image1', 'image2', 'image3', 'image4', 'image5'), extra=1)

# class ExhaustHoodForm(forms.ModelForm):
#     class Meta:

#         model = Product
#         fields = ['name','product_group','brand', 'product_model_no', 'ventilation_type', 'in_the_box', 'body_material', 'dimension', 'suction_capacity', 'wattage', 'warranty', 'description']
#         widgets = {
#             'name': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'product_group': forms.Select( attrs={'class': 'form-control'}, choices=PRODUCT_GROUP_CHOICES),
#             'brand': forms.Select( attrs={'class': 'form-control'}, choices=PRODUCT_BRAND_CHOICES),
#             'product_model_no': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'ventilation_type': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'in_the_box': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'body_material': forms.Select( attrs={'class': 'form-control'}, choices=MATERIAL_CHOICES),
#             'dimesnion': forms.Select( attrs={'class': 'form-control'}, choices=MATERIAL_CHOICES),
#             'suction_capacity': forms.Select( attrs={'class': 'form-control'}, choices=MATERIAL_CHOICES),
#             'wattage': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'warranty': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'description': forms.Textarea( attrs={'class': 'form-control', 'rows':3}),
#            }

# PriceFormSet = inlineformset_factory(Product, Price, form= ExhaustHoodForm, fields=('minimum_quantity', 'maximum_quantity', 'price_per_piece', 'color', 'image1', 'image2', 'image3', 'image4', 'image5'), extra=1)


# class HandBladerForm(forms.ModelForm):
#     class Meta:

#         model = Product
#         fields = ['name','product_group','brand', 'product_model_no', 'in_the_box', 'body_material', 'blade_material', 'no_of_speed', 'dimension', 'wattage', 'warranty', 'description']
#         widgets = {
#             'name': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'product_group': forms.Select( attrs={'class': 'form-control'}, choices=PRODUCT_GROUP_CHOICES),
#             'brand': forms.Select( attrs={'class': 'form-control'}, choices=PRODUCT_BRAND_CHOICES),
#             'product_model_no': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'in_the_box': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'body_material': forms.Select( attrs={'class': 'form-control'}, choices=MATERIAL_CHOICES),
#             'blade_material': forms.Select( attrs={'class': 'form-control'}, choices=MATERIAL_CHOICES),
#             'no_of_speed': forms.Select( attrs={'class': 'form-control'}, choices=MATERIAL_CHOICES),
#             'wattage': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'warranty': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'description': forms.Textarea( attrs={'class': 'form-control', 'rows':3}),
#            }

# PriceFormSet = inlineformset_factory(Product, Price, form= HandBladerForm, fields=('minimum_quantity', 'maximum_quantity', 'price_per_piece', 'color', 'image1', 'image2', 'image3', 'image4', 'image5'), extra=1)

# class KitchenStovesForm(forms.ModelForm):
#     class Meta:

#         model = Product
#         fields = ['name','product_group','brand', 'product_model_no', 'in_the_box', 'body_material', 'product_weight', 'wattage', 'warranty', 'description']
#         widgets = {
#             'name': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'product_group': forms.Select( attrs={'class': 'form-control'}, choices=PRODUCT_GROUP_CHOICES),
#             'brand': forms.Select( attrs={'class': 'form-control'}, choices=PRODUCT_BRAND_CHOICES),
#             'product_model_no': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'in_the_box': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'body_material': forms.Select( attrs={'class': 'form-control'}, choices=MATERIAL_CHOICES),
#             'product_weight': forms.Select( attrs={'class': 'form-control'}, choices=MATERIAL_CHOICES),
#             'wattage': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'warranty': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'description': forms.Textarea( attrs={'class': 'form-control', 'rows':3}),
#            }

# PriceFormSet = inlineformset_factory(Product, Price, form= KitchenStovesForm, fields=('minimum_quantity', 'maximum_quantity', 'price_per_piece', 'color', 'image1', 'image2', 'image3', 'image4', 'image5'), extra=1)


# class MixersGrindersForm(forms.ModelForm):
#     class Meta:

#         model = Product
#         fields = ['name','product_group','brand', 'product_model_no', 'in_the_box', 'body_material', 'no_of_jars', 'no_of_speed', 'wattage', 'warranty', 'description']
#         widgets = {
#             'name': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'product_group': forms.Select( attrs={'class': 'form-control'}, choices=PRODUCT_GROUP_CHOICES),
#             'brand': forms.Select( attrs={'class': 'form-control'}, choices=PRODUCT_BRAND_CHOICES),
#             'product_model_no': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'in_the_box': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'body_material': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'no_of_jars': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'no_of_speeds': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'wattage': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'warranty': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'description': forms.Textarea( attrs={'class': 'form-control', 'rows':3}),
#            }

# PriceFormSet = inlineformset_factory(Product, Price, form= MixersGrindersForm, fields=('minimum_quantity', 'maximum_quantity', 'price_per_piece', 'color', 'image1', 'image2', 'image3', 'image4', 'image5'), extra=1)


# class MixersGrindersForm(forms.ModelForm):
#     class Meta:

#         model = Product
#         fields = ['name','product_group','brand', 'product_model_no', 'in_the_box', 'body_material', 'capacity', 'wattage', 'warranty', 'description']
#         widgets = {
#             'name': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'product_group': forms.Select( attrs={'class': 'form-control'}, choices=PRODUCT_GROUP_CHOICES),
#             'brand': forms.Select( attrs={'class': 'form-control'}, choices=PRODUCT_BRAND_CHOICES),
#             'product_model_no': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'in_the_box': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'body_material': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'capacity': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'wattage': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'warranty': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'description': forms.Textarea( attrs={'class': 'form-control', 'rows':3}),
#            }

# PriceFormSet = inlineformset_factory(Product, Price, form= MixersGrindersForm, fields=('minimum_quantity', 'maximum_quantity', 'price_per_piece', 'color', 'image1', 'image2', 'image3', 'image4', 'image5'), extra=1)


# class SandWichMakerForm(forms.ModelForm):
#     class Meta:

#         model = Product
#         fields = ['name','product_group','brand', 'product_model_no', 'in_the_box', 'body_material', 'product_weight', 'wattage', 'warranty', 'description']
#         widgets = {
#             'name': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'product_group': forms.Select( attrs={'class': 'form-control'}, choices=PRODUCT_GROUP_CHOICES),
#             'brand': forms.Select( attrs={'class': 'form-control'}, choices=PRODUCT_BRAND_CHOICES),
#             'product_model_no': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'in_the_box': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'body_material': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'product_weight': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'wattage': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'warranty': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'description': forms.Textarea( attrs={'class': 'form-control', 'rows':3}),
#            }

# PriceFormSet = inlineformset_factory(Product, Price, form= SandWichMakerForm, fields=('minimum_quantity', 'maximum_quantity', 'price_per_piece', 'color', 'image1', 'image2', 'image3', 'image4', 'image5'), extra=1)


# class ToastersForm(forms.ModelForm):
#     class Meta:

#         model = Product
#         fields = ['name','product_group','brand', 'product_model_no', 'in_the_box', 'body_material', 'product_weight', 'no_of_slices', 'wattage', 'warranty', 'description']
#         widgets = {
#             'name': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'product_group': forms.Select( attrs={'class': 'form-control'}, choices=PRODUCT_GROUP_CHOICES),
#             'brand': forms.Select( attrs={'class': 'form-control'}, choices=PRODUCT_BRAND_CHOICES),
#             'product_model_no': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'in_the_box': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'body_material': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'product_weight': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'no_of_slices': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'wattage': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'warranty': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'description': forms.Textarea( attrs={'class': 'form-control', 'rows':3}),
#            }

# PriceFormSet = inlineformset_factory(Product, Price, form= ToastersForm, fields=('minimum_quantity', 'maximum_quantity', 'price_per_piece', 'color', 'image1', 'image2', 'image3', 'image4', 'image5'), extra=1)


# class WaterFilterPurifiersForm(forms.ModelForm):
#     class Meta:

#         model = Product
#         fields = ['name','product_group','brand', 'product_model_no', 'in_the_box', 'body_material', 'tank_capacity', 'product_type', 'wattage', 'warranty', 'description']
#         widgets = {
#             'name': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'product_group': forms.Select( attrs={'class': 'form-control'}, choices=PRODUCT_GROUP_CHOICES),
#             'brand': forms.Select( attrs={'class': 'form-control'}, choices=PRODUCT_BRAND_CHOICES),
#             'product_model_no': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'in_the_box': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'body_material': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'tank_capacity': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'product_type': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'wattage': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'warranty': forms.TextInput( attrs={'class': 'form-control', 'id':''}),
#             'description': forms.Textarea( attrs={'class': 'form-control', 'rows':3}),
#            }

# PriceFormSet = inlineformset_factory(Product, Price, form= WaterFilterPurifiersForm, fields=('minimum_quantity', 'maximum_quantity', 'price_per_piece', 'color', 'image1', 'image2', 'image3', 'image4', 'image5'), extra=1)
