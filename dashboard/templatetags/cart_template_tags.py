from django import template
from product.models import Order
from accounts.models import Account
from product.models import Product, ShopCart

register = template.Library()


@register.filter
def cart_product_count(user):
    if user.is_authenticated:
        qs = ShopCart.objects.filter(user=user)
        if qs.exists():
            return qs.count()
    return 0

@register.filter
def get_slug(request):
    product = Product.objects.filter(user__account__slug=slug)
    return "dilip"
