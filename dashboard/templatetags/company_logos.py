from django import template
from dashboard.models import SellerCompany

register = template.Library()


@register.filter
def logo(user):
    if user.is_authenticated:
        qs = SellerCompany.objects.get(user=user)
        if qs:
            return qs.logo.url
    return None