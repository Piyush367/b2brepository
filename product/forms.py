from .models import Product, SitePreview
from  django import forms

class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = "__all__"
        exclude = ['user']


PAYMENT_CHOICES = (
    # ('S', 'Stripe'),
    # ('COD', 'Cash On Delivery'),
    ('P', 'Online'),
)


class CheckoutForm(forms.Form):
    set_default_shipping = forms.BooleanField(required=False)
    use_default_shipping = forms.BooleanField(required=False)

    set_default_billing = forms.BooleanField(required=False)
    use_default_billing = forms.BooleanField(required=False)

    address_building = forms.CharField(max_length=100)    
    address_area = forms.CharField(max_length=100)
    landmark = forms.CharField(max_length=50)
    locality = forms.CharField(max_length=50)
    city = forms.CharField(max_length=50)
    phone = forms.CharField(max_length=50)
    state = forms.CharField(max_length=100)
    zip = forms.CharField(max_length=100)
    
    payment_option = forms.ChoiceField(
        widget=forms.RadioSelect, choices=PAYMENT_CHOICES)


class CouponForm(forms.Form):
    code = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Promo code',
        'aria-label': 'Recipient\'s username',
        'aria-describedby': 'basic-addon2'
    }))

class SitePreviewForm(forms.ModelForm):
    background = forms.CharField(widget=forms.HiddenInput(),required=False)
    button = forms.CharField(widget=forms.HiddenInput(),required=False)
    logo = forms.ImageField(widget=forms.HiddenInput(),required=False)
    banner_image = forms.ImageField(widget=forms.HiddenInput(),required=False)

    class Meta:
        model = SitePreview
        fields = ('background', 'button', 'logo', 'banner_image')

