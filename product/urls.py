from django.urls import path
from . import views
from django.views.generic import TemplateView

app_name="product"

urlpatterns = [
    path("handlerequest/", views.handlerequest, name="HandleRequest"),
    path("back/", views.back, name="back"),
    path("preview/<str:slug>/",views.SitePreviewView.as_view(), name="preview"),


    path("",views.HomeView, name="home_list"),
    path("category/",views.category, name="category"),
    path("<str:slug>/",views.WebsiteHomeList.as_view(), name="website_home_list"),

    path("product/checkout/",views.CheckoutView.as_view(), name="product_checkout"),
    path("product/order-track/<int:pk>/",views.ProductTrackView.as_view(), name="product_order_track"),

    path("product/add-to-cart/<str:slug>/<int:pk>/",views.AddToCart.as_view(), name="add_to_cart"),
    
    path("product/cart-summary/",views.CartView.as_view(), name="cart_summary"),

    path("product/remove-from-cart/<int:pk>/",views.RemoveFromCartView.as_view(), name="remove_from_cart"),
    path("product/remove-single-item-from-cart/<int:pk>/",views.RemoveSingleItemFromCartView.as_view(), name="remove_single_item_from_cart"),
    path("product/add-single-item-to-cart/<int:pk>/",views.AddSingleItemToCartView.as_view(), name="add_single_item_to_cart"),
    ####compare route########

    path("product/compare-product-list/",views.CompareView.as_view(), name="compare_view"),

    path("product/remove-compare-product/<int:pk>/",views.RemoveFromCompareView.as_view(), name="remove_from_compare"),
    # path("product/remove-single-item-compare-product/<int:pk>/",views.RemoveSingleItemFromCompareView.as_view(), name="remove_single_item_from_compare"),
    path("product/add-single-item-to-compare-product/<int:pk>/",views.AddSingleItemToCompareView.as_view(), name="add_single_item_to_compare"),

    ]
