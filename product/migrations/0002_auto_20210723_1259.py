# Generated by Django 2.2.15 on 2021-07-23 07:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='price',
            name='color',
            field=models.CharField(max_length=255),
        ),
        migrations.AlterField(
            model_name='price',
            name='name',
            field=models.CharField(max_length=555),
        ),
        migrations.AlterField(
            model_name='price',
            name='slug',
            field=models.CharField(default='dilip', max_length=255),
        ),
        migrations.AlterField(
            model_name='product',
            name='in_the_box',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AlterField(
            model_name='product',
            name='product_group',
            field=models.CharField(blank=True, choices=[('Kitchen Stoves', 'Kitchen Stoves'), ('Mixer Grinder', 'Mixer Grinder'), ('Rice Cookers', 'Rice Cookers'), ('Electric Kettles', 'Electric Kettles'), ('OTG', 'OTG'), ('Juicers', 'Juicers'), ('Blenders', 'Blenders'), ('Water Heaters', 'Water Heaters'), ('Water Filters', 'Water Filters'), ('Induction Cookers', 'Induction Cookers'), ('Exhaust Hoods', 'Exhaust Hoods'), ('Sandwich Makers', 'Sandwich Makers'), ('Toaster', 'Toaster'), ('Deep Fryers', 'Deep Fryers'), ('Coffee Makers', 'Coffee Makers'), ('Electric Iron', 'Electric Iron'), ('Vaccum Cleaner', 'Vaccum Cleaner'), ('Air Purifiers', 'Air Purifiers'), ('Exhaust Fans', 'Exhaust Fans'), ('Room Heaters', 'Room Heaters'), ('Table Fans', 'Table Fans'), ('Wall Fans', 'Wall Fans'), ('Electric Steamers & Vaporizers', 'Electric Steamers & Vaporizers'), ('Air Coolers', 'Air Coolers')], max_length=50, null=True),
        ),
    ]
