from django.db import models
from django.contrib.auth.models import User
from django.shortcuts import reverse
from django.conf import settings
from django.contrib.auth.models import User
from django.utils.html import mark_safe

PRODUCT_GROUP_CHOICES = (
    ("Kitchen Stoves", "Kitchen Stoves"),
    ("Mixer Grinder","Mixer Grinder"),
    ("Rice Cookers", "Rice Cookers"),
    ("Electric Kettles", "Electric Kettles"),
    ("OTG", "OTG"),
    ("Juicers", "Juicers"),
    ("Blenders", "Blenders"),
    ("Water Heaters", "Water Heaters"),
    ("Water Filters", "Water Filters"),
    ("Induction Cookers", "Induction Cookers"),
    ("Exhaust Hoods", "Exhaust Hoods"),
    ("Sandwich Makers", "Sandwich Makers"),
    ("Toaster", "Toaster"),
    ("Deep Fryers", "Deep Fryers"),
    ("Coffee Makers", "Coffee Makers"),
    ("Electric Iron", "Electric Iron"),
    ("Vaccum Cleaner", "Vaccum Cleaner"),
    ("Air Purifiers", "Air Purifiers"),

    ("Exhaust Fans", "Exhaust Fans"),
    ("Room Heaters", "Room Heaters"),
    ("Table Fans", "Table Fans"),
    ("Wall Fans", "Wall Fans"),
    ("Electric Steamers & Vaporizers", "Electric Steamers & Vaporizers"),
    ("Air Coolers", "Air Coolers"),
    )

CAPACITY_CHOICES = (
    ("200ml", "200 ml"),
    ("500ml", "500 ml"),
    ("1ltr", "1 ltr")
)

MATERIAL_CHOICES = (
    ("polycarbonate", "Polycarbonate"),
    ("PET", "PET"),
    ("other", "Other")
)

Brand_CHOICES = (
    ("prestige", "Prestige"),
    ("padmini", "Padmini"),
    ("other", "Other")
)

class Color(models.Model):
    title=models.CharField(max_length=100)
    color_code=models.CharField(max_length=100)
    is_draft = models.BooleanField(default=False)

    class Meta:
        verbose_name_plural='4. Colors'

    def color_bg(self):
        return mark_safe('<div style="width:30px; height:30px; background-color:%s"></div>' % (self.color_code))

    def __str__(self):
        return self.title


class Product(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    slug= models.CharField(max_length=255,default='',blank=True, null=True)
    name = models.CharField(max_length=255)
    product_group = models.CharField(max_length=120, choices=PRODUCT_GROUP_CHOICES, null=True,blank=True)
    brand = models.CharField(max_length=50, null=True,blank=True)
    product_model_no = models.CharField(max_length=50, null=True,blank=True)
    product_type = models.CharField(max_length=50, null=True,blank=True)
    in_the_box = models.CharField(max_length=50, null=True,blank=True)
    body_material = models.CharField(max_length=50, null=True,blank=True)
    capacity = models.CharField(max_length=50, null=True,blank=True)
    wattage = models.CharField(max_length=50, null=True,blank=True)
    warranty = models.CharField(max_length=50, null=True,blank=True)
    description = models.TextField()

    plate_material = models.CharField(max_length=50, null=True,blank=True)
    blade_material = models.CharField(max_length=50, null=True,blank=True)
    product_weight = models.CharField(max_length=50, null=True,blank=True)
    coverage_area = models.CharField(max_length=50, null=True,blank=True)

    product_length = models.CharField(max_length=50, null=True,blank=True)
    product_width = models.CharField(max_length=50, null=True,blank=True)

    cooktop_material = models.CharField(max_length=50, null=True,blank=True)

    no_of_speed = models.CharField(max_length=50, null=True,blank=True)

    tank_capacity = models.CharField(max_length=50, null=True,blank=True)
    fan_material = models.CharField(max_length=50, null=True,blank=True)

    ventilation_type = models.CharField(max_length=50, null=True,blank=True)
    dimension = models.CharField(max_length=50, null=True,blank=True)
    suction_capacity = models.CharField(max_length=50, null=True,blank=True)
    
    no_of_jars = models.CharField(max_length=50, null=True,blank=True)

    no_of_slices = models.CharField(max_length=50, null=True,blank=True)
    
    arrange = models.BooleanField(default=False)
    add_home = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    def color_bg(self):
        return mark_safe('<div style="width:30px; height:30px; border-color: coral;border-style: solid;border-radius: 25px; background-color:%s"></div>' % (self.color))

    # def image_tag(self):
    #     product = self.objects.all()

    #     price = Price.objects.raw('SELECT * FROM  product_price  WHERE product_id IN(SELECT id from product_product) GROUP BY product_id')
        # return mark_safe('<img src="%s" width="50px;" height="auto" />' % (price.image1.url))


    # def get_absolute_url(self):
    #     return reverse("dashboard:seller_product_detail", kwargs={
    #         'pk': self.pk
    #     })

    def get_absolute_url(self):
        return reverse("dashboard:seller_product_detail", kwargs={
            'slug': self.slug
        })

    def get_add_to_cart_url(self):
        return reverse("dashboard:add-to-cart", kwargs={
            'pk': self.pk
        })

    def get_remove_from_cart_url(self):
        return reverse("dashboard:remove-from-cart", kwargs={
            'pk': self.pk
        })

class Price(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    slug = models.CharField(max_length=255, default="dilip")
    name = models.CharField(max_length=555)
    minimum_quantity = models.PositiveIntegerField(null=True)
    maximum_quantity = models.PositiveIntegerField(null=True)
    price_per_piece = models.PositiveIntegerField(null=True)
    color = models.CharField(max_length=255)
    image1 = models.ImageField(upload_to="products/%Y/%m/%d/", null=True,blank=True)
    image2 = models.ImageField(upload_to="products/%Y/%m/%d/", null=True,blank=True)
    image3 = models.ImageField(upload_to="products/%Y/%m/%d/", null=True,blank=True)
    image4 = models.ImageField(upload_to="products/%Y/%m/%d/", null=True,blank=True)
    image5 = models.ImageField(upload_to="products/%Y/%m/%d/", null=True,blank=True)
    quantity = models.PositiveIntegerField(null=True,blank=True)
    amount = models.PositiveIntegerField(null=True,blank=True)

    def __str__(self):
        return self.product.name

    def get_minimum_quantity(self):
        return self.minimum_quantity
    
    def get_maximum_quantity(self):
        return self.maximim_quantity

    def get_price_per_piece(self):
        return self.price_per_piece
    
    def get_quantity(self):
        return self.get_quantity

    def get_price(self):
        return self.price

    def get_color(self):
        return self.color

    def get_no_of_colors(self):
        return(len(self.color))
    
    def get_total_price(self):
        grand_total = 0
        for i in range (self.get_no_of_colors):
            total = self.get_price*self.get_price_per_piece
            grand_total += total
        return grand_total
    
    @property
    def image1_url(self):
        if self.image1 and hasattr(self.image1, 'url'):
            return self.image1.url

    @property
    def image2_url(self):
        if self.image2 and hasattr(self.image2, 'url'):
            return self.image2.url

    @property
    def image3_url(self):
        if self.image3 and hasattr(self.image3, 'url'):
            return self.image3.url

    @property
    def image4_url(self):
        if self.image4 and hasattr(self.image4, 'url'):
            return self.image4.url
    
    @property
    def image5_url(self):
        if self.image5 and hasattr(self.image5, 'url'):
            return self.image5.url

    def image1_tag(self):
        return mark_safe('<img src="%s" width="50px;" height="auto" />' % (self.image1.url))

    def image2_tag(self):
        return mark_safe('<img src="%s" width="50px;" height="auto" />' % (self.image2.url))

    def image3_tag(self):
        return mark_safe('<img src="%s" width="50px;" height="auto" />' % (self.image3.url))

    def image4_tag(self):
        return mark_safe('<img src="%s" width="50px;" height="auto" />' % (self.image4.url))

    def image5_tag(self):
        return mark_safe('<img src="%s" width="50px;" height="auto" />' % (self.image5.url))
        
    def color_bg(self):
        return mark_safe('<div style="width:30px; height:30px; border-color: coral;border-style: solid;border-radius: 25px; background-color:%s"></div>' % (self.color))

# class OrderProduct(models.Model):
#     user = models.ForeignKey(User, on_delete=models.CASCADE, null=True,blank=True)
#     seller_id = models.IntegerField(default=0)
#     ordered = models.BooleanField(default=False)
#     product = models.ForeignKey(Product, on_delete=models.CASCADE, null=True,blank=True)
#     quantity = models.IntegerField(default=0)

#     def __str__(self):
#         return f"{self.quantity} of {self.product.name}"

#     def get_total_product_price(self):
#         return self.quantity * self.product.price

#     def get_total_discount_product_price(self):
#         return self.quantity * self.product.discount_price

#     def get_amount_saved(self):
#         return self.get_total_product_price() - self.get_total_discount_product_price()

#     def get_final_price(self):
#         if self.product.discount_price:
#             return self.get_total_discount_product_price()
#         return self.get_total_product_price()

# STATUS_CHOICES = (
#     ('Order confirmed', 'Order confirmed'),
#     ('Processing order', 'Processing order'),
#     ('Quality check', 'Quality check'),
#     ('Product dispatched', 'Product dispatched'),
#     ('Product delivered', 'Product delivered'),
# )

# class Order(models.Model):
#     user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user", null=True,blank=True)
#     seller = models.ForeignKey(User, on_delete=models.CASCADE, related_name="seller", null=True,blank=True)
#     products = models.ManyToManyField(OrderProduct)
#     shipping_address = models.ForeignKey('Address', related_name='shipping_address', on_delete=models.SET_NULL, blank=True, null=True)
#     start_date = models.DateTimeField(auto_now_add=True)
#     ordered_date = models.DateTimeField()
#     payment = models.ForeignKey("Payment", on_delete=models.SET_NULL, blank=True, null=True)
#     coupon = models.ForeignKey("Coupon", on_delete=models.SET_NULL, blank=True, null=True)
#     refund_requested = models.BooleanField(default=False)
#     refund_granted = models.BooleanField(default=False)
#     ordered = models.BooleanField(default=False)
#     payment_mode = models.CharField(max_length=20, default='NOT PROCESSED')
#     ref_code = models.CharField(max_length=20, blank=True, null=True)
#     status = models.CharField(max_length=285, choices=STATUS_CHOICES, default="Order confirmed")

#     def __str__(self):
#         return self.user.username

#     def get_total(self):
#         total = 0
#         for order_product in self.products.all():
#             total += order_product.get_final_price()
#         return total

class Address(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,on_delete=models.CASCADE)
    phone = models.IntegerField()
    address_building = models.CharField(max_length=100)
    address_area = models.CharField(max_length=100)
    landmark = models.CharField(max_length=50)
    locality = models.CharField(max_length=50)
    city = models.CharField(max_length=50)
    state = models.CharField(max_length=100)
    zip = models.CharField(max_length=100)
    default = models.BooleanField(default=False)

    def __str__(self):
        return self.user.username

    class Meta:
        verbose_name_plural = 'Addresses'

class Coupon(models.Model):
    code = models.CharField(max_length=15)
    amount = models.FloatField()

    def __str__(self):
        return self.code

class Payment(models.Model):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, blank=True, null=True)
    order_id = models.CharField(max_length=255)
    txn_id = models.CharField(max_length=255)
    txn_amount = models.CharField(max_length=255)
    pmt_mode = models.CharField(max_length=255)
    txn_date = models.DateTimeField(auto_now_add=True)
    txn_status = models.CharField(max_length=255)

    def __str__(self):
        return self.user.username


class ShopCart(models.Model):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, related_name="user")
    seller = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, related_name="seller")
    product = models.ForeignKey(Product, on_delete=models.SET_NULL, null=True)
    price = models.ForeignKey(Price, on_delete=models.SET_NULL,blank=True, null=True)
    quantity = models.IntegerField(null=True)
    amount = models.PositiveIntegerField(null=True)
    color = models.CharField(max_length=20, null=True)

    def __str__(self):
        return self.product.name
    
    def get_amount(self):
        return (self.price.price_per_piece*self.quantity)
    
    def get_total(self):
        amt = 0
        for i in ShopCart.objects.all():
            amt+=i.amount
        return amt

    def color_bg(self):
        return mark_safe('<div style="width:30px; height:30px; border-color: coral;border-style: solid;border-radius: 25px; background-color:%s"></div>' % (self.color))

    def image1_tag(self):
        return mark_safe('<img src="%s" width="50px;" height="auto" />' % (self.price.image1.url))


    # @property
    # def price(self):
    #     return (self.product.price)

    # @property
    # def amount(self):
    #     return (self.quantity * self.product.price)

    # @property
    # def varamount(self):
    #     return (self.quantity * self.variant.price)

STATUS_CHOICES = (
    ('Order confirmed', 'Order confirmed'),
    ('Processing order', 'Processing order'),
    ('Quality check', 'Quality check'),
    ('Product dispatched', 'Product dispatched'),
    ('Product delivered', 'Product delivered'),
)
class OrderProduct(models.Model):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, related_name="order_user")
    # seller = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, related_name="order_seller")
    shipping_address = models.ForeignKey(Address, on_delete=models.CASCADE, null=True)
    status=models.CharField(max_length=120, choices=STATUS_CHOICES,default='Order confirmed')
    create_at=models.DateTimeField(auto_now_add=True)
    update_at=models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.user.first_name
    
    def get_shipping_address(self):
        return self.shipping_address.state, self.shipping_address.city


class Order(models.Model):
    order_product = models.ForeignKey(OrderProduct, on_delete=models.CASCADE, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE,related_name="user1")
    seller = models.ForeignKey(User, on_delete=models.CASCADE,related_name="seller1")
    product = models.ForeignKey(Product, on_delete=models.CASCADE, null=True)
    price = models.ForeignKey(Price, on_delete=models.SET_NULL,blank=True, null=True)
    payment = models.ForeignKey(Payment, on_delete=models.CASCADE, null=True)
    address = models.ForeignKey(Address, on_delete=models.CASCADE, null=True)
    quantity = models.IntegerField(null=True)
    amount = models.FloatField()
    color = models.CharField(max_length=20, null=True)
    status = models.CharField(max_length=120, choices=STATUS_CHOICES, default='Order confirmed')
    create_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.product.name
    
   
    def min_price(self):
        return str(self.price.minimum_quantity)+ '/-'

    def max_price(self):
        return str(self.price.maximum_quantity)+ '/-'
    
    def address(self):
        return self.order_product.shipping_address.city, self.order_product.shipping_address.state

    def color_bg(self):
        return mark_safe('<div style="width:30px; height:30px; border-color: coral;border-style: solid;border-radius: 25px; background-color:%s"></div>' % (self.color))

    def image1_tag(self):
        return mark_safe('<img src="%s" width="50px;" height="auto" />' % (self.price.image1.url))

    def get_total(self):
        grand_total = 0
        for i in Order.objects.all():
            grand_total+=i.amount
        return grand_total

class SitePreview(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True, default=1)
    background = models.CharField(max_length=20)
    button = models.CharField(max_length=20)
    logo = models.ImageField(upload_to="logo_banner")
    banner_image = models.ImageField(upload_to="logo_banner")

    def __str__(self):
        return self.user.username
class CompareProduct(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE,null=True)
    product = models.ForeignKey(Product, on_delete=models.CASCADE,null=True)    

    class Meta:
        db_table = 'compare_product'
        verbose_name = ("Compare Product")
        verbose_name_plural = ("Compare Products")

    def __str__(self):
       return self.user.username


