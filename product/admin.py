from django.contrib import admin
from .models import *



@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'user', 'slug', 'product_group', 'brand')
<<<<<<< HEAD
=======
    prepopulated_fields = {'slug': ('name',)}
>>>>>>> d1b97b0fb238df1f15884ed799e4ff6b9878695f

@admin.register(Price)
class PriceAdmin(admin.ModelAdmin):
    list_display = ('id', 'product_id', 'product', 'minimum_quantity', 'maximum_quantity', 'price_per_piece', 'color', 'color_bg', 'image1_tag', 'image2_tag', 'image3_tag', 'image4_tag', 'image5_tag')

@admin.register(ShopCart)
class ShopCartAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'seller', 'product', 'price', 'quantity', 'amount', 'color', 'color_bg', 'image1_tag')

@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'address', 'seller', 'product', 'min_price', 'max_price', 'quantity', 'amount', 'status', 'color', 'color_bg', 'image1_tag')

@admin.register(Address)
class AddressAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'phone', 'address_building', 'address_area', 'landmark', 'locality', 'city', 'state', 'zip', 'default')

@admin.register(OrderProduct)
class OrderProductAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'get_shipping_address', 'status', 'create_at', 'update_at')

@admin.register(Payment)
class PaymentAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'txn_id', 'txn_amount', 'pmt_mode', 'txn_date', 'txn_status')

admin.site.register(SitePreview)