from django.shortcuts import render, get_object_or_404, redirect
from . models import Product,Payment,Address, CompareProduct
from dashboard.models import SellerCompany, BusinessProfile
from django.views.generic import ListView, DetailView
from django.contrib.auth.models import User
from django.http import HttpResponse
from accounts.models import *
from itertools import chain
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.urls import reverse
import json
from django.views import View
from django.db.models import Q
from django.core import serializers
from django.http import JsonResponse
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from .models import Order, Address
from django.views.decorators.csrf import csrf_exempt
import datetime
import random
from django.utils import timezone
import string
from PayTm import Checksum
from accounts.models import Account
from dashboard.models import SellerProfile, Color
from subscription.models import Subscription
from .forms import CheckoutForm, CouponForm

from datetime import date
from dateutil.relativedelta import relativedelta
from django.core.exceptions import ObjectDoesNotExist

mid = 'fGrIzA97726764689450'
merchent_key ='NjeNJa4DJmq53!yu'

def create_ref_code():
    return ''.join(random.choices(string.ascii_lowercase + string.digits, k=20))


def products(request):
    context = {
        'product': Product.objects.all()
    }
    return render(request, "products.html", context)

def is_valid_form(values):
    valid = True
    for field in values:
        if field == '':
            valid = False
    return valid


id_brand=[]
id_category=[]

def HomeView(request):
    brand=[]
    brand_id=[]
    cat=[]
    cat_id_obj=[]
    prd_name=[]
    prd=Product.objects.all()
    for i in prd:
        if(i.brand not in brand):
            brand.append(i.brand)
            brand_id.append(i.id)
        if(i.product_group not in cat):
            cat.append(i.product_group)
            cat_id_obj.append(i.id)
        if(i.name not in prd_name):
            prd_name.append(i.name)

    brand=json.dumps(list(brand))
    brand_id=json.dumps(list(brand_id))
    cat_data=json.dumps(list(cat))
    cat_id=json.dumps(list(cat_id_obj))
    prd_name=json.dumps(list(prd_name))

    if request.method =='POST' and request.POST['action']=='list':
        print('liiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii')
        global id_brand
        global id_category
        id_brand=request.POST.getlist('brand[]')
        id_category=request.POST.getlist('cat[]')


    if request.method =='POST' and request.POST['action']=='send_var':
        print('lllllllllllllllllllllllllllllllllllllllllllllllllllllllllllll')
        alt=request.POST.get('alt')
        print(alt)
        nav = Product.objects.filter(product_group=alt)
        print(nav)
        if not nav.exists():
            return JsonResponse({"valid":False}, status = 400)
        else:
            nav = list(nav)
            id_category.clear()
            id_brand.clear()
            for i in nav:
                nav_id="category"+str(i.id)
                id_category.append(nav_id)
            return JsonResponse({"valid":True}, status = 200)




    # object_list=Product.objects.filter(add_home=True).order_by("?")[:8]
    object_list = Price.objects.raw('SELECT * FROM  product_price  WHERE product_id IN(SELECT id from product_product) GROUP BY product_id')[:8]
    # from django.db.models import Count
    # distinct = Price.objects.values('product_id').annotate(product_id_count=Count('product_id')
    # ).filter(product_id_count=1)
    # print(distinct)
    # object_list = Price.objects.filter(product_id__in=[item['product_id'] for item in distinct])
    # object_list = Price.objects.order_by().values('product_id', 'name', 'image1').distinct()
    price = Price.objects.all()
    l = []
    for p in price:
        l.append(p.product_id)
    print("Dilip", set(l))

    x = list(object_list)
    l = len(x)
    i=0
    while(l%4!=0):
        x.append(x[i])
        l=l+1
        i=i+1
    print(x)


    if request.method =='POST' and request.POST['action']=='location':
        loc=request.POST.get('loc')
        print('cccccccccccccccccccccccccccccccccccccccccccccccccccc')

        data=Product.objects.filter(Q(brand=loc) | Q(product_group=loc) | Q(name=loc))
        data=Price.objects.filter(product_id__in= data)
        # l =[]
        # for d in data:
        #     # datas = Product.objects.get(pk=5)
        #     d1 = d.price_set.all()
        #     for i in d1:
        #         l.append(i)
        #         print("dddddddddddddddddddd", i)
        # l=l[0]
        # print("listssssssssssssssssssssss",l.image1)

        xx=serializers.serialize('json',list(data))
        x=serializers.serialize('json',x)
        context={
            'loc_data':xx,
            'queryset':x,
            }
        return JsonResponse(context)

    
    pricdoct = Price.objects.raw('SELECT * FROM  product_price  WHERE product_id IN(SELECT id from product_product) GROUP BY product_id')

    context={'brand_data':brand,
             'brand_id':brand_id,
             'x':x,
             'object_list':object_list,
             'cat_data':cat_data,
             'cat_id':cat_id,
             'prd_name':prd_name,
             'pricdoct' :pricdoct,
             # 'subscription': subscription
             }
    try:
        context['subscription'] = Subscription.objects.filter(user = request.user, status=True).last()
    except:
        pass
    return render(request,'index.html', context) 

def category(request):
    if request.method =='POST' and request.POST['action']=='brand_cat':
        print("Helloooooooooooooooooooooooo")
        global id_brand
        global id_category
        id_brand=request.POST.getlist('brand[]')
        id_category=request.POST.getlist('cat[]')
    show_res=[]
    queryset=[]
    if(len(id_brand)>0):
        show_res=id_brand
    elif(len(id_category)>0):
        show_res=id_category
    temp=[]
    temp=show_res
    show_res=[]
    for i in temp:
        show_res.append(i.replace("category",""))
    show_res=list(dict.fromkeys(show_res))
    for i in show_res:
        # product_detail=Product.objects.filter(id=i)
        product_detail=Price.objects.filter(product_id=i)
        queryset.append(product_detail)
    # temp=[]
    # for i in queryset:
    #     for j in i:
    #         temp.append(j.name)
    #         temp.append(j.brand)
    #         temp.append(j.product_group)
    # from itertools import chain
    # items2 = (chain.from_iterable(queryset))

    prod_cat = ""
    for i in (chain.from_iterable(queryset)):
        # prod_cat = i.product_group
        prod_cat = i.product.product_group
        prod_cat = prod_cat.upper()

    brand=[]
    brand_id=[]
    cat=[]
    cat_id_obj=[]
    prd=Product.objects.all()
    for i in prd:
        if(i.brand not in brand):
            brand.append(i.brand)
            brand_id.append(i.id)
        if(i.product_group not in cat):
            cat.append(i.product_group)
            cat_id_obj.append(i.id)
    brand=json.dumps(list(brand))
    brand_id=json.dumps(list(brand_id))
    cat_data=json.dumps(list(cat))
    cat_id=json.dumps(list(cat_id_obj))
    # product = Product.objects.filter().order_by("?")[:4]
    product = Price.objects.raw('SELECT * FROM  product_price  WHERE product_id IN(SELECT id from product_product) GROUP BY product_id')[:4]
    # product1 = Product.objects.filter().order_by("?")[:8]
    product1 = Price.objects.raw('SELECT * FROM  product_price  WHERE product_id IN(SELECT id from product_product) GROUP BY product_id')[:8]

    if request.method =='POST' and request.POST['action']=='cat_location':
        print("loccccccccccccccccccccccccccccccccccccccccccccc")
        loc=request.POST.get('cat_loc')
        append_search=[]
        data=Product.objects.filter(Q(brand=loc) | Q(product_group=loc) | Q(name=loc))
        context={'append_search':serializers.serialize('json',list(data))}
        return JsonResponse(context)

    # if request.method =='GET' :
    #     val = request.GET.get('inp')
    #     print("hekkk",val)
    #     if val !=None:
    #         product = Account.objects.filter(state = val)
    #         queryset=[]
    #         for i in product:
    #             prd = Product.objects.filter(id = i.id)
    #             # prod = Product.objects.filter(user_id = i.id)
    #             # prd = Price.objects.filter(product_id = prod)
    #             queryset.append(prd)
    
    if request.method =='GET' :
        val = request.GET.get('inp')
        print("hekkk",val)
        if val !=None:
            product = Account.objects.filter(state = val)
            queryset=[]
            for i in product:
                prd = Product.objects.filter(user__account__id = i.id)
                prd = Price.objects.filter(product__user__account__id=i.id)
                print('prffffffffffffffffffffffffffffffffffffffffff', prd)
                queryset.append(prd)


    print("previoussssssssssssssssssssssssss", queryset)
    temp=[]
    for i in queryset:
        for j in i:
        # try:
            temp.append(j.product.name)
            temp.append(j.product.brand)
            temp.append(j.product.product_group)
        # except:
        #     temp.append(j.name)
        #     temp.append(j.brand)
        #     temp.append(j.product_group)


    items2 = (chain.from_iterable(queryset))
    items3 = (chain.from_iterable(queryset))
    x = [len(queryset)]*len(queryset)
    print("Dilip Sapkotaaaaaaaaaaaaaaaaaaa", items2)
    print("Dilip Sapkotaaaaaaaaaaaaaaaaaaa", x)
    length = len(queryset)
    context={'items2':zip(items2,x),
            'items3':zip(items3,x),
            'items_js':json.dumps(temp),
            'length':length,
            'search_product':queryset,
             'brand_data':brand,
             'brand_id':brand_id,
             'cat_data':cat_data,
             'cat_id':cat_id,
             'product':product,
             'product1':product1,
             'prod_cat':prod_cat,
            }
    return render(request,'category.html',context)




################################## this is for displaying the home page ##################################
# class HomeView(ListView):
#     model = Product
#     template_name = "index.html"

    # def get_queryset(self):
        # return Product.objects.filter(add_home=True)
    #     # return Product.objects.raw('SELECT  DISTINCT user_id from product_product WHERE add_home=True')
    #     return Product.objects.order_by('user_id').values_list('user_id', flat=True).distinct()

from .models import SitePreview
from .forms import SitePreviewForm
class WebsiteHomeList(ListView):
    model = Product    
    template_name = "dashboard/company.html"
    # template_name = "dashboard/company_preview.html"
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = get_object_or_404(User, account__slug=self.kwargs.get("slug"))

        try:
            context['color'] = Color.objects.get(user__account__slug=self.kwargs['slug'])
        except:
            pass
        try:
            context['user'] = (self.request.user.account.slug == self.kwargs['slug'])
        except:
            pass
        

        business_profile = SitePreview.objects.filter(user=user)[0]
       
        # context['object_list'] = Product.objects.filter(user=user)
        context['object_list'] = Price.objects.filter(user=user)[:6]
        # context['object_list'] = Price.objects.raw('SELECT * FROM  product_price  WHERE product_id IN(SELECT id from product_product) GROUP BY product_id')[:4]

        # context['object_list'] = Color.objects.filter(product__user=user,product__arrange=True)
        context['all_object_list'] = Price.objects.filter(user=user)[:6]
        # context['all_object_list'] = Product.objects.filter(user=user)
        # context['all_object_list'] = Price.objects.raw('SELECT * FROM  product_price  WHERE user_id IN(SELECT user_id from product_product) GROUP BY product_id')[:4]
        # context['all_object_list'] = Price.objects.raw('SELECT * FROM  product_price  WHERE product_id IN(SELECT product_id from product_product) GROUP BY product_id')[:4]
        context['seller_company'] = SellerCompany.objects.filter(user=user)
        
        context['business_profile'] = BusinessProfile.objects.filter(user=user)
        context['contact_profile'] = SellerProfile.objects.filter(user=user, approve=True)
            
        return context


class SitePreviewView(View):
    def get(self, *args, **kwargs):
        user = get_object_or_404(User, account__slug=self.kwargs.get("slug"))
        object_list = Price.objects.raw('SELECT * FROM  product_price  WHERE product_id IN(SELECT id from product_product) GROUP BY product_id')[:4]

        # context['object_list'] = Color.objects.filter(product__user=user,product__arrange=True)
        # all_object_list = Product.objects.filter(user=user)
        object_list = Price.objects.raw('SELECT * FROM  product_price  WHERE product_id IN(SELECT id from product_product) GROUP BY product_id')[:4]
    
        seller_company = SellerCompany.objects.filter(user=user)
        contact_profile = SellerProfile.objects.filter(user=user, approve=True)

        business_profile = SitePreview.objects.filter(user=user)[0]
        form = SitePreviewForm(instance=business_profile)
        context = {
                'business_profile': business_profile,
                'object_list': object_list,
                # 'all_object_list': all_object_list,
                'seller_company': seller_company,
                'business_profile': business_profile,
                'contact_profile': contact_profile,
                'form' :form
                
        }
        return render(self.request, 'dashboard/company_preview.html', context)
############################################Checkout###################################################
# class CheckoutView(View):
#     def get(self, *args ,**kwargs):
#         return render(self.request, 'product/checkout.html')
from .models import OrderProduct, Order
class CheckoutView(View):
    def get(self, *args, **kwargs):
        try:
            shop_cart = ShopCart.objects.filter(user=self.request.user)
            shop_cart1 = ShopCart.objects.filter(user=self.request.user).first()
            form = CheckoutForm()

            context = {
                'form': form,
                'couponform': CouponForm(),
                'shop_cart': shop_cart,
                'shop_cart1': shop_cart1,
                'DISPLAY_COUPON_FORM': True
            }

            shipping_address_qs = Address.objects.filter(
                user=self.request.user,
                # address_type='S',
                default=True
            )
            if shipping_address_qs.exists():
                context.update(
                    {'default_shipping_address': shipping_address_qs[0]})

            return render(self.request, "product/checkout.html", context)
        except ObjectDoesNotExist:
            messages.info(self.request, "You do not have an active order")
            return redirect("product:cart")

    def post(self, *args, **kwargs):
        form = CheckoutForm(self.request.POST or None)
        try:
            order = ShopCart.objects.filter(user=self.request.user)
            shop_cart = ShopCart.objects.filter(user = self.request.user)
            if form.is_valid():
                use_default_shipping = form.cleaned_data.get(
                    'use_default_shipping')
                if use_default_shipping:
                    print("Using the defualt shipping address")
                    address_qs = Address.objects.filter(
                        user=self.request.user,
                        # address_type='S',
                        default=True
                    )
                    if address_qs.exists():
                        shipping_address = address_qs[0]
                        order_product = OrderProduct()  
                        order.shipping_address = shipping_address
                        order.save()
                    else:
                        messages.info(
                            self.request, "No default shipping address available")
                        return redirect('product:cart')
                else:
                    print("User is entering a new shipping address")
                    phone = form.cleaned_data.get('phone')
                    address_building = form.cleaned_data.get('address_building')
                    address_area = form.cleaned_data.get('address_area')
                    landmark = form.cleaned_data.get('landmark')
                    locality = form.cleaned_data.get('locality')
                    city = form.cleaned_data.get('city')
                    state = form.cleaned_data.get('state')
                    zip = form.cleaned_data.get('zip')
                    if is_valid_form([phone, address_building, address_area, landmark, locality, city, state, zip]):
                        try:
                            print("tryyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy",phone,address_building,address_area,locality,city,state,zip)
                            shipping_address = Address.objects.filter(user=self.request.user)
                            shipping_address.phone=phone
                            shipping_address.address_building=address_building
                            shipping_address.address_area=address_area
                            shipping_address.landmark=landmark
                            shipping_address.locality=locality
                            shipping_address.city=city
                            shipping_address.state=state
                            shipping_address.zip=zip
                            shipping_address.save()
                            print("end tryyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy")
                        except:
                            print("Excepttttttttttttttttttttttttttttttttttt")
                            shipping_address = Address(
                                user=self.request.user,
                                phone=phone,
                                address_building=address_building,
                                address_area=address_area,
                                landmark=landmark,
                                locality=locality,
                                city=city,
                                state=state,
                                zip=zip,
                                # address_type='S'
                            )
                            shipping_address.save()
                        order_product = OrderProduct() 
                        order_product.user_id = self.request.user.id
                        # order_product.seller = shopcart.product.user,
                        order_product.shipping_address = shipping_address
                        order_product.save()
                        
                        for sc in shop_cart:
                            order = Order()
                            order.order_product_id = order_product.id
                            order.user_id = self.request.user.id
                            order.seller_id = sc.product.user.id
                            order.product_id   = sc.product_id
                            order.price_id = sc.price_id
                            order.quantity = sc.quantity
                            order.amount = sc.amount
                            order.color = sc.color
                            order.save()

                        set_default_shipping = form.cleaned_data.get(
                            'set_default_shipping')
                        print('dilip :', set_default_shipping)
                        if set_default_shipping:
                            shipping_address.default = True
                            shipping_address.save()

                    else:
                        messages.info(self.request, "Please fill in the required shipping address fields")
                        return redirect('core:checkout')
###############################################################################################################
                payment_option = form.cleaned_data.get('payment_option')
                print('payment option :', payment_option)
                if payment_option == 'COD':
                    return redirect('core:paymentcod', payment_option='COD')
                elif payment_option == 'P':
                    order = Order.objects.filter(user=self.request.user).last()
                    current_time = str(datetime.datetime.now())
                    updated_id = ''.join([current_time[:10],current_time[11:13],current_time[14:16],current_time[17:], str(order.id)])

                    param_dict = {
                        'MID': mid,
                        'ORDER_ID': str(updated_id),
                        'TXN_AMOUNT': str(order.get_total()),
                        'CUST_ID': self.request.user.email,
                        'INDUSTRY_TYPE_ID': 'Retail',
                        'WEBSITE': 'WEBSTAGING',
                        'CHANNEL_ID': 'WEB',
                        'CALLBACK_URL': 'http://127.0.0.1:8000/handlerequest/',

                    }
                    param_dict['CHECKSUMHASH'] = Checksum.generate_checksum(param_dict, merchent_key)
                    return render(self.request, 'paytm/paytm.html', {'param_dict': param_dict})

                else:
                    messages.warning(
                        self.request, "Invalid payment option selected")
                    return redirect('product:cart')
        except ObjectDoesNotExist:
            messages.warning(self.request, "You do not have an active order")
            return redirect("product:cart")

@csrf_exempt
def handlerequest(request):
    # paytm will send you post request here
    form = request.POST
    response_dict = {}
    for i in form.keys():
        response_dict[i] = form[i]
    return render(request, 'paytm/paymentstatus.html', {'response': response_dict})

@csrf_exempt
@login_required
def back(request):
    if request.method == 'POST':
        form = request.POST
        response_dict = {}
        for i in form.keys():
            response_dict[i] = form[i]
            if i == 'CHECKSUMHASH':
                checksum = form[i]
        verify = Checksum.verify_checksum(response_dict, merchent_key, checksum)

        if verify:
            if response_dict['RESPCODE'] == '01':
                # create the payment
                print("inside backkkkkkkkkkkkkkkkkkkkkkkkkkkk verify")
                payment = Payment()
                order_id_post = form['ORDERID']
                in_order_id_post = int(order_id_post[23:])
                payment.order_id = in_order_id_post
                payment.txn_id = form['TXNID']
                payment.txn_amount = form['TXNAMOUNT']
                payment.pmt_mode = form['PAYMENTMODE']
                payment.txn_date = form['TXNDATE']
                payment.txn_status = form['STATUS']
                payment.user = request.user
                obj = Payment.objects.filter(user=request.user, order_id=int(order_id_post[23:]))
                if not obj.exists():
                    payment.save()
                    
                    # assign the payment to the order
                    # order = Order.objects.get(id=in_order_id_post)
                    # order_items = order.products.all()
                    # order_items.update(ordered=True)
                    # for item in order_items:
                    #     item.save()
                    # order.ordered = True
                    # order.ordered_date = timezone.now()
                    # order.payment_mode = 'ONLINE'
                    # order.payment = payment
                    # order.payment.pmt_mode='ONLINE'
                    # order.ref_code = create_ref_code()
                    # order.save()
                    ShopCart.objects.filter(user=request.user).delete() # Clear & Delete shopcart
                    print('order successful')
                    messages.info(request, 'Your order is successful')
            else:
                print('order was not successful because' + response_dict['RESPMSG'])
                messages.warning(request, 'Your Order is Failure')
        print("outsideeeeeeeeeeeeeeeeeeeeeeeeeeeee", response_dict)
        return render(request, 'paytm/back.html',{'response':response_dict})
    return redirect('dashboard:cart')

class PaymentcodView(View):
    def get(self, *args, **kwargs):
        order = Order.objects.get(user=self.request.user, ordered=False)
        if order.shipping_address:
            context = {
                'order': order,
                'DISPLAY_COUPON_FORM': False
            }
            return render(self.request, "payment_cod.html", context)
        else:
            messages.warning(
                self.request, "You have not added a shipping address")
            return redirect("core:checkout")

    def post(self, *args, **kwargs):
        order = Order.objects.get(user=self.request.user, ordered=False)

        order.ref_code = create_ref_code()
        order.save()

        # payment method
        payment = Payment()
        payment.user = self.request.user
        payment.amount = order.get_total()
        payment.save()

        # assign the payment to the order

        order_items = order.items.all()
        order_items.update(ordered=True)
        for item in order_items:
            item.save()

        order.ordered = True
        order.payment = payment
        order.ordered_date = timezone.now()
        order.payment_mode = 'COD'
        order.ref_code = create_ref_code()
        order.save()
        # payment method ends
        messages.success(self.request, "Your order was successful!")
        # return redirect("/payment/COD/")
        return redirect("/")

class OrderSummaryView(LoginRequiredMixin, View):
    def get(self, *args, **kwargs):
        try:
            order = Order.objects.get(user=self.request.user, ordered=False)
            context = {
                'object': order
            }
            return render(self.request, 'order_summary.html', context)
        except ObjectDoesNotExist:
            messages.warning(self.request, "You do not have an active order")
            return redirect("/")

class ProductTrackListView(ListView):
    model = Order
    template_name = 'company/ordertrack_list.html'

    def get_queryset(self,*args, **kwargs):
        return Order.objects.filter(ordered=True, user=self.request.user)

class ProductTrackView(View):
    def get(self,*args, **kwargs):
        order = Order.objects.get(pk=self.kwargs['pk'])
        print("sellerrrrrrrrrrrrrrrr", order, order.status)
        # order = Order.objects.filter(ordered=True)[0]
        if order.status=="Order confirmed":
            percentage = 20

        if order.status == "Processing order":
            percentage = 40

        if order.status == "Quality check":
            percentage = 60

        if order.status == "Product dispatched":
            percentage = 80

        if order.status == "Product delivered":
            percentage = 100
        print(percentage)
        return render (self.request, 'company/ordertrack.html', {'percentage':percentage,'order':order})

class AddCouponView(View):
    def post(self, *args, **kwargs):
        form = CouponForm(self.request.POST or None)
        if form.is_valid():
            try:
                code = form.cleaned_data.get('code')
                order = Order.objects.get(
                    user=self.request.user, ordered=False)
                order.coupon = get_coupon(self.request, code)
                order.save()
                messages.success(self.request, "Successfully added coupon")
                return redirect("core:checkout")
            except ObjectDoesNotExist:
                messages.info(self.request, "You do not have an active order")
                return redirect("core:checkout")
    
############################################################################################

from .models import Price, ShopCart
class AddToCart(View):
    def post(self, *args, **kwargs):
        url = self.request.META.get('HTTP_REFERER')
        user = self.request.user
        # accepting all thhing  in post method
        price = self.request.POST
        # getting values
        l = price.values()
        # converting into list
        li = list(l)
        # removing csrf token and taking only ids
        li = li[1:]
        print('printingggggggggggggg', li)
        # checking whether product is selected or not
        if not li:
            messages.info(self.request, "please select the product")
            return redirect(self.request.META.get('HTTP_REFERER'))
        # for i in range(len(li)):
        #     globals()['string%s' % i] = Price.objects.get(id__in = li[i])
        # print("string1", string0)s

        product = Product.objects.get(user__account__slug=self.kwargs.get("slug"), pk=self.kwargs.get("pk"))
        for i in range(len(li)):
            string = Price.objects.get(id = li[i])
            print(string.color)
            check_cart = ShopCart.objects.filter(product = product, user = user, seller=product.user, price=string, color=string.color)  # Check product in shopcart
        if check_cart:
            for i in range(len(li)):
                product = Product.objects.get(user__account__slug=self.kwargs.get("slug"), pk=self.kwargs.get("pk"))
                string = Price.objects.get(id = li[i])
                shopcart = ShopCart.objects.filter(user=user)
                shopcart.price=string, 
                shopcart.quantity = string.minimum_quantity, 
                # shopcart.amount=int(string.price_per_piece)*int(string.quantity), 
                # shopcart.save()

        else : # Inser to Shopcart
            for i in range(len(li)):
                string = Price.objects.get(id = li[i])
                data, create = ShopCart.objects.get_or_create(user=user, seller=product.user, product=product, price=string, quantity=string.minimum_quantity, amount=int(string.price_per_piece)*int(string.minimum_quantity), color=string.color)
            
        return redirect("product:cart_summary")
    
class CartView(LoginRequiredMixin, View):
    def get(self, *args, **kwargs):
        try:
            shop_cart = ShopCart.objects.filter(user=self.request.user)
            order = ShopCart.objects.filter(user=self.request.user).first()
            context = {
                'object': order,
                'shop_cart': shop_cart,
            }
            return render(self.request, 'product/cart.html', context)
        except ObjectDoesNotExist:
            messages.warning(self.request, "You do not have an active order")
            return redirect("/")
    
class RemoveFromCartView(View):
    def get(self, *args, **kwargs):
        ShopCart.objects.get(pk=self.kwargs['pk']).delete()
        return redirect(self.request.META.get('HTTP_REFERER'))

class RemoveSingleItemFromCartView(View):
    def post(self, *args, **kwargs):
        minimum_quantity = self.request.POST['minimum_quantity']
        maximum_quantity = self.request.POST['maximum_quantity']
        price_per_piece = self.request.POST['price_per_piece']
        color = self.request.POST['color']
        try:
            print("tryyyyyyyyyyyy")
            # product = Product.objects.get(pk=self.kwargs['pk'])
            shop_cart = ShopCart.objects.get(id=self.kwargs['pk'])

            print("pkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk", shop_cart.quantity, shop_cart.price.minimum_quantity)
            # shop_cart = ShopCart.objects.get(price=price)
            print("sssssssssssssssssssssssssssss")
            if int(shop_cart.price.minimum_quantity)+10 <=int(shop_cart.quantity):
                print("again ifffffffffffffffff")
                shop_cart.quantity-=10
                shop_cart.save()
                shop_cart.amount = shop_cart.price.price_per_piece*shop_cart.quantity
                print("Dilippppppppppppppppppppppppp")
                shop_cart.save()
                return redirect(self.request.META.get('HTTP_REFERER'))
            else:
                pass
                
        except:
            pass
        return redirect(self.request.META.get('HTTP_REFERER'))

class AddSingleItemToCartView(View):
    def post(self, *args, **kwargs):
        minimum_quantity = self.request.POST['minimum_quantity']
        maximum_quantity = self.request.POST['maximum_quantity']
        price_per_piece = self.request.POST['price_per_piece']
        color = self.request.POST['color']
        try:
            print("tryyyyyyyyyyyy")
            # product = Product.objects.get(pk=self.kwargs['pk'])
            shop_cart = ShopCart.objects.get(id=self.kwargs['pk'])

            print("pkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk", shop_cart.quantity, shop_cart.price.minimum_quantity, shop_cart.price.maximum_quantity)
            # shop_cart = ShopCart.objects.get(price=price)
            print("sssssssssssssssssssssssssssss")
            # if int(shop_cart.price.maximum_quantity) <= int(shop_cart.quantity):
            #     print("again ifffffffffffffffff")
            shop_cart.quantity+=10
            shop_cart.save()
            shop_cart.amount = shop_cart.price.price_per_piece*shop_cart.quantity
            print("Dilippppppppppppppppppppppppp")
            shop_cart.save()
            return redirect(self.request.META.get('HTTP_REFERER'))
            # else:
            #     pass
                
        except:
            pass
        return redirect(self.request.META.get('HTTP_REFERER'))


# class CheckOutView(View):
#     pass

#################Compare Product##################

class CompareView(LoginRequiredMixin, View):
    def get(self, *args, **kwargs):
        compare_product = ''
        try:            
            compare_product = CompareProduct.objects.filter(user=self.request.user).select_related('product','user')
            context = {
                'compare_product': compare_product,
            }
            return render(self.request, 'product/compare.html', context)
        except ObjectDoesNotExist:
            messages.warning(self.request, "You do not have compare product")
            return redirect("/")
    
class RemoveFromCompareView(LoginRequiredMixin, View):
    def post(self, *args, **kwargs):
        body = json.loads(self.request.body)
        cpId = body['cpId']
        try:
          CompareProduct.objects.get(pk=cpId).delete()          
        except CompareProduct.DoesNotExist:
          return JsonResponse({'status':False, 'message':'Not exists in compare product list'}, status=404)
        return JsonResponse({'status':True, 'message':'Deleted compare product'}, status=200)
        

class AddSingleItemToCompareView(LoginRequiredMixin, View):
    def post(self, *args, **kwargs):
        body = json.loads(self.request.body)
        pro_id = body['pro_id']
        if self.request.user.is_authenticated:
            total = CompareProduct.objects.filter(user=self.request.user).count()
            # return HttpResponse(total)
            if int(total) > 5:            
                if CompareProduct.objects.filter(product=pro_id, user=self.request.user).exists():
                    return JsonResponse({'status':False, 'message':'Allready exists in compare product list'}, status=404)
                return JsonResponse({'status':False, 'message':'Allow only 6 product to compare'}, status=404)
            try:
                product = Product.objects.get(id=pro_id)
                compare_product = CompareProduct.objects.create(
                    product = product,
                    user = self.request.user,
                )
                compare_product.save() 
                return JsonResponse({'status':True, 'message':'Add to compare successfully'}, status=200)
            except Product.DoesNotExist:
                return JsonResponse({'status':False, 'message':'Product does not exists'}, status=404)
        else:
            return JsonResponse({'status':False, 'message':'Unauthorized access'}, status=401)
        


